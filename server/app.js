const express = require('express');
// Creating Express.js app
const app = express();

const connectToDB = require('./configurations/database');
// Connecting to the DB
connectToDB();

// This middleware is for using req.body
app.use(express.json({ extended: false }));

// Frontend can access these resources 
const path = require('path');
app.use("/pictures", express.static(path.join("database/pictures")));

// Cors Headers
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

    if (req.method === 'OPTIONS') {
        res.header(
            'Access-Control-Allow-Methods',
            'OPTIONS, GET, POST, PUT, PATCH, DELETE'
        );

        return res.status(200).json({});
    }

    next();
});

const passport = require('passport'),
    jwtStrategy = require('./configurations/jwt-strategy');
// Setting passport and jwtStrategy
app.use(passport.initialize());
passport.use(jwtStrategy);

const authentication = require('./middlewares/authentication');
const usersRoutes = require('./routes/users/users');
const authorsRoutes = require('./routes/authors');
const genresRoutes = require('./routes/genres');
const booksRoutes = require('./routes/books');
const discussionsRoutes = require('./routes/discussions');
const shelvesRoutes = require('./routes/shelves');
// Setting up routes
app.use('/users', usersRoutes);
app.use(authentication.checkAuthenticated);
app.use('/authors', authorsRoutes);
app.use('/genres', genresRoutes);
app.use('/books', booksRoutes);
app.use('/discussions', discussionsRoutes);
app.use('/shelves', shelvesRoutes);

const notFound = require('./middlewares/not-found'),
    methodNotAllowed = require('./middlewares/method-not-allowed'),
    errorHandler = require('./middlewares/error-handler');
const Book = require('./models/book');
// Get non existing routes
app.get('/*', notFound);
// Any other request
app.use(methodNotAllowed);
// Handling all errors
app.use(errorHandler);


module.exports = app;