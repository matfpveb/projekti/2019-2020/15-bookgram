const mongoose = require('mongoose');


const connectToDB = (db = 'bookgram') => {
    mongoose.connect(`mongodb://127.0.0.1:27017/${db}`, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        })
        .then(() => console.log(`Connected to the database ${db}`))
        .catch(err => {
            console.error(err);
            process.exit(1);
        });
};


module.exports = connectToDB;