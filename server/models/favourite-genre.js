const mongoose = require('mongoose');

const User = require('./user'),
    Genre = require('./genre');


const favouriteGenreSchema = mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: User.modelName,
        required: true
    },
    genres: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: Genre.modelName
    }]
});


favouriteGenreSchema.statics.addToFavouriteGenres = async function(userId, genreId) {
    await this.updateOne({ user: userId }, { $addToSet: { genres: genreId } }, {
        upsert: true
    }).exec();
};

favouriteGenreSchema.statics.removeFromFavouriteGenres = async function(userId, genreId) {
    await this.updateOne({ user: userId }, { "$pull": { genres: genreId } }).exec();
}

favouriteGenreSchema.statics.checkIfInFavourites = async function(userId, genreId) {
    const genreExists = await this.exists({ user: userId, genres: genreId });

    return genreExists;
};

favouriteGenreSchema.statics.getFavouriteGenresByUser = async function(userId) {
    const favouriteGenres = await this.findOne({ user: userId }, { genres: 1, _id: 0 }).populate('genres').exec();

    return favouriteGenres;
}

const FavouriteGenre = mongoose.model('FavouriteGenre', favouriteGenreSchema);

module.exports = FavouriteGenre;