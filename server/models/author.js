const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');


const authorSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    bio: {
        type: String,
        required: true
    },
    imagePath: {
        type: String
    }
});

authorSchema.index({ name: 'text', bio: 'text' });
authorSchema.plugin(mongoosePaginate);

authorSchema.statics.getAuthors = async function() {
    const authors = await this.find({}, { 'name': 1 }).exec();

    return authors;
}

authorSchema.statics.getAllAuthors = async function(options) {
    const authors = await this.paginate({}, options);

    return authors;
}


authorSchema.statics.searchAuthors = async function(options, keywords) {
    options.select = { score: { $meta: "textScore" } };
    options.sort = { score: { $meta: "textScore" } };
    options.lean = true;

    const authors = await this.paginate({ $text: { $search: keywords } }, options);

    return authors;
}

authorSchema.statics.getAuthorById = async function(authorId) {
    const author = await this.findById(authorId).exec();

    return author ? Promise.resolve(author) : Promise.reject();
}


const Author = mongoose.model('Author', authorSchema);

module.exports = Author;