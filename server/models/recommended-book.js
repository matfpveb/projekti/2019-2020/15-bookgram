const mongoose = require('mongoose');

const User = require('./user'),
    Book = require('./book'),
    BookRating = require('./book-rating');

var math = require('mathjs');


const recommendedBookSchema = mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: User.modelName,
        required: true
    },
    books: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: Book.modelName
    }]
});

recommendedBookSchema.statics.getBooks = async function(userId) {
    //await User.getUserById(userId);

    /*
        Knjige se preporucuju pomocu jednostavnog recommender sistema koji je zasnovan na 
        kolaborativnom filtriranju. (Kod je zasnovan na kodu sa vezbi iz NI - prilagodjen je 
        obliku nasih podataka i peske su izracunti delovi koji bi se inace racunali pomocu
        funkcija iz sklearn, numpy, ...)
    */

    /* Prvo brisemo sve prethodene preporuke */
    this.deleteAll(userId);

    const bookRatingsByUser = await BookRating.getByUser(userId);
    const books = await Book.getAll();
    const users = await User.getAllUsers();

    num_of_users = users.length;
    num_of_books = books.length;

    user_ids = users.map(user => { return user._id });
    user_indexes = Array.from(Array(num_of_users).keys());

    book_ids = books.map(book => { return book._id });
    
    var user_id_index = {};
    user_ids.forEach((user_id, i) => user_id_index[user_id] = user_indexes[i]);

    
    /* Ako korisnik kome treba da predlozimo knjige nije ocenio nijednu knjigu, 
       preporucicemo mu 5 knjiga sa najvecim prosecnim ocenama */
    if (bookRatingsByUser.length === 0) {

        bookid_rate = {}

        avgRates = [];
        for (var i = 0; i < num_of_books; i++) {
            const avgRate = await BookRating.getAverageRating(books[i]._id);
            bookid_rate[books[i]._id] = avgRate;
        }

        // Pravimo niz oblika [[book_id, rating]]
        var items = Object.keys(bookid_rate).map(function(key) {
            return [key, bookid_rate[key]];
        });

        // Sortiramo opadajuce rejtingu
        items.sort(function(first, second) {
            return second[1] - first[1];
        });

        // Kreiramo niz koji sadrzi 5 knjiga sa najvecom prosecnom ocenom
        top5 = items.slice(0, 5);
        recommended_book_ids = top5.map((item) => { return item[0]; });

        recommended_book_ids.forEach((recommended_book_id) => {
            this.addBook(userId, recommended_book_id);
        });

        const recommendedBooks = await this.find({ user: userId }).populate('books').exec();

        return recommendedBooks;

    }


    /*  
        data_matrix je matrica dimenzija num_of_users x num_of_books.
        Element na poziciji (i,j) oznacava vrednost ocene korisnika i za knjigu j.
        Preostali elementi matrice su 0.
    */
    data_matrix = Array(num_of_users).fill(0).map(() => Array(num_of_books).fill(0));

    for (var j = 0; j < num_of_books; j++) {

        const bookRatings = await BookRating.getAll(book_ids[j]);

        if (bookRatings[0] != undefined) {
            var num_of_ratings = bookRatings[0].ratings.length;
            for (var k = 0; k < num_of_ratings; k++) {
                user_id = bookRatings[0].ratings[k].user._id;
                i = user_id_index[user_id];
                data_matrix[i][j] = bookRatings[0].ratings[k].rating;
            }
        }

    }

    //console.log(data_matrix);


    /*
        similarity_matrix je matrica dimenzija num_of_users x num_of_users.
        Element na poziciji (i,j) pokazuje koliko su slicni korisnici i i j.
    */
    similarity_matrix = Array(num_of_users).fill(0).map(() => Array(num_of_users).fill(0));

    for (var i = 0; i < num_of_users; i++) {
        for (var j = 0; j < num_of_users; j++) {
            user1_ratings = data_matrix[i]
            user2_ratings = data_matrix[j]

            user1_norm = math.sqrt(user1_ratings.map((rating) => { return rating * rating }).reduce((a, b) => a + b, 0));
            user2_norm = math.sqrt(user2_ratings.map((rating) => { return rating * rating }).reduce((a, b) => a + b, 0));

            norm_product = parseFloat(user1_norm * user2_norm);
            if (norm_product == 0) {
                /* Ovo se desava ako postoji korisnik koji nije ocenio nijednu knjigu. */
                similarity_matrix[i][j] = 0;
                similarity_matrix[j][i] = 0;
            } else {
                similarity = math.dot(user1_ratings, user2_ratings) / norm_product;
                similarity_matrix[i][j] = similarity;
                similarity_matrix[j][i] = similarity;
            }
        }
    }

    //console.log(similarity_matrix);


    /* 
        user_book_predictions je niz dimenzija num_of_books.
        Element na poziciji j predstavlja predikiciju sistema za to 
        koliko ce se datom korisniku dopasti knjiga j.
    */

    var i = user_id_index[userId];  // user index
    user_book_predictions =  Array(num_of_books).fill(0);

    for (var j = 0; j < num_of_books; j++) {

        X = data_matrix[i];
        Xi_mean = X.reduce((a, b) => a + b, 0) / X.length

        weighted_similarity = 0;
        total_similarity = 0;

        // za svakog korisnika racunamo doprinos sumama
        for (var k = 0; k < num_of_users; k++) {
            if (k != i) {
                Xk = data_matrix[k];
                Xk_mean = Xk.reduce((a, b) => a + b, 0) / Xk.length
                weighted_similarity += similarity_matrix[i][k] * (data_matrix[k][j] - Xk_mean)
                total_similarity += similarity_matrix[i][k]
            }
        }

        if (total_similarity != 0) {
            user_book_predictions[j] = Xi_mean + weighted_similarity / total_similarity
        } 
    }
    

    books_read_by_user = bookRatingsByUser.map((bookRatingByUser) => { return bookRatingByUser.book._id; });
    books_read_by_user = books_read_by_user.map((bookId) => new String(bookId));

    /* Ako je korisnik citao knjige koje nijedan drugi korisnik nije procitao,
       onda knjige ne mogu da mu se preporuce na osnovu slicnosti sa drugim korisnicima.
       Onda ce mu samo biti preporucene neke knjige iz zanrova koje je citao. */
    if(user_book_predictions.every(prediction => prediction == 0)){
        genreIds_read_by_user = [];
        for (var i = 0; i < books_read_by_user.length; i++){
            const book = await Book.getBookById(books_read_by_user[i].valueOf());
            const genreId = new String(book['genre']);
            genreIds_read_by_user.push(genreId.valueOf());
            
        }
        genreIds_read_by_user = [...new Set(genreIds_read_by_user)]; // remove duplicates
        
        recommended_book_ids = [];

        for (var i = 0; i < genreIds_read_by_user.length; i++){
            books_from_genre = await Book.getBooksByGenre(genreIds_read_by_user[i].valueOf());
            books_from_genre = books_from_genre.map(book => new String(book._id));
            books_from_genre.forEach((bookId) => {
                if (!books_read_by_user.some(book => book.valueOf() == bookId.valueOf())) {  
                    recommended_book_ids.push(bookId.valueOf());
                }
            });
        }   

        // preporucicemo max 10 knjiga
        n = 10 < recommended_book_ids.length ? 10 : recommended_book_ids.length;
        recommended_book_ids = recommended_book_ids.slice(0, n);
        
        recommended_book_ids.forEach((recommended_book_id) => {
            this.addBook(userId, recommended_book_id);
        });
        const recommendedBooks = await this.find({ user: userId }).populate('books').exec();
        return recommendedBooks;
    }


    recommended_book_ids = [];
    for (var i = 0; i < num_of_books; i++) {
        prediction = user_book_predictions[i];
        if (prediction > 0.9) {
            var recommended_book_id = new String(book_ids[i]);
            /* Ako korisnik nije citao tu knjigu, preporucicemo mu je: */
            if (!books_read_by_user.some(book => book.valueOf() == recommended_book_id.valueOf())) {  
                recommended_book_ids.push(recommended_book_id);
            }
        }
    }

    //console.log(recommended_book_ids);

    recommended_book_ids.forEach((recommended_book_id) => {
        this.addBook(userId, recommended_book_id);
    });

    const recommendedBooks = await this.find({ user: userId }).populate('books').exec();

    return recommendedBooks;
};

recommendedBookSchema.statics.addBook = async function(userId, bookId) {
    //await Book.getById(bookId);

    await this.updateOne({ user: userId }, { $addToSet: { books: bookId } }, {
        new: true,
        upsert: true
    }).exec();
};


recommendedBookSchema.statics.deleteAll = async function(userId) {
    await this.deleteMany({ user: userId }).exec();
};


const RecommendedBook = mongoose.model('RecommendedBook', recommendedBookSchema);

module.exports = RecommendedBook;