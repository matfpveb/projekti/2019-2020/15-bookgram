const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');


const genreSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    }
});

genreSchema.index({ name: 'text', description: 'text' });
genreSchema.plugin(mongoosePaginate);


genreSchema.statics.getGenresList = async function() {
    const genres = await this.find({}).exec();

    return genres;
}

genreSchema.statics.getAllGenres = async function() {
    const genres = await this.find({}).lean().exec();

    return genres;
};

genreSchema.statics.getGenreById = async function(genreId) {
    const genre = await this.findById(genreId).exec();

    return genre ? Promise.resolve(genre) : Promise.reject();
}

genreSchema.statics.searchGenres = async function(keywords) {
    const genres = await this.find({ $text: { $search: keywords } }, { score: { $meta: "textScore" } }).sort({ score: { $meta: "textScore" } }).lean().exec();

    const options = {};
    options.select = { score: { $meta: "textScore" } };
    options.sort = { score: { $meta: "textScore" } };
    options.lean = true;
}

const Genre = mongoose.model('Genre', genreSchema);

module.exports = Genre;