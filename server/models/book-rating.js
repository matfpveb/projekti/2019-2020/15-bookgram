const mongoose = require('mongoose');

const Book = require('../models/book');
const User = require('../models/user');

const bookRatingSchema = new mongoose.Schema({
    book: {
        type: mongoose.Schema.Types.ObjectId,
        ref: Book,
        required: true
    },
    ratings: [{
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: User,
            required: true
        },
        rating: {
            type: Number
        },
        postedAt: {
            type: Date,
            default: Date.now
        }
    }]
});


bookRatingSchema.statics.addRating = async function(bookId, userId, rating) {
    // Check that the book exists
    await Book.getBookById(bookId);

    // If there is already a rating for this book from this user
    // change it
    const change = await this.updateOne({ 'ratings.user': userId, book: bookId }, { $set: { 'ratings.$.rating': rating, 'ratings.$.postedAt': Date.now() } });

    // If this book has never been rated
    // or this user hasn't rated this book
    // create new rating
    if (!change.n) {
        await this.updateOne({ book: bookId }, { $addToSet: { ratings: { user: userId, rating } } }, {
            upsert: true
        });
    }
};

bookRatingSchema.statics.getRatings = async function(bookId) {
    const ratings = await this.findOne({ book: bookId }).exec();

    return ratings;
};

bookRatingSchema.statics.getAll = async function(bookId) {
    const bookRatings = await this.find({ 'book': bookId })
        .populate('ratings.user', 'username')
        .populate('book', 'title').exec();

    return bookRatings;
};

bookRatingSchema.statics.getUserRating = async function(bookId, userId) {
    const rating = await this.findOne({ book: bookId, 'ratings.user': userId }, { 'ratings.$': 1, '_id': 0 }).exec();

    return rating;
}

bookRatingSchema.statics.getAverageRating = async function(bookId) {
    const avgRating = await this.aggregate([
        { $match: { book: mongoose.Types.ObjectId(bookId) } },
        { $project: { avgRating: { $avg: "$ratings.rating" } } }
    ]).exec();

    return avgRating;
}

bookRatingSchema.statics.getByUser = async function(userId) {
    await User.getUserById(userId);

    const bookRatings = await this.find({ 'ratings.user': userId }, { 'ratings.$': 1 })
        .populate('book', 'title')
        .exec();

    return bookRatings;
};


const BookRating = mongoose.model('BookRating', bookRatingSchema);

module.exports = BookRating;