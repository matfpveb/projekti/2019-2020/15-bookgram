const mongoose = require('mongoose');

const Book = require('./book');
const User = require('./user');


const bookReviewSchema = mongoose.Schema({
    book: {
        type: mongoose.Schema.Types.ObjectId,
        ref: Book,
        required: true
    },
    reviews: [{
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: User,
            required: true
        },
        review: {
            type: String
        },
        postedAt: {
            type: Date,
            default: Date.now
        }
    }]
});


bookReviewSchema.statics.addReview = async function(bookId, userId, review) {
    await Book.getBookById(bookId);

    // If there is already a review for this book from this user
    // change it
    const change = await this.updateOne({ 'reviews.user': userId, book: bookId }, { $set: { 'reviews.$.review': review, 'reviews.$.postedAt': Date.now() } });

    // If this book has never been reviewed 
    // or this user hasn't reviewed this book
    // create new review
    if (!change.n) {
        await this.updateOne({ book: bookId }, { $addToSet: { reviews: { user: userId, review } } }, {
            upsert: true
        });
    }
};

bookReviewSchema.statics.getReviews = async function(bookId) {
    const reviews = await this.findOne({ book: bookId }, { 'reviews': 1, '_id': 0 })
        .populate('reviews.user', '_id username imagePath').exec();

    return reviews;
};

bookReviewSchema.statics.removeReview = async function(bookId, reviewId) {
    await this.updateOne({ book: bookId, 'reviews._id': reviewId }, { $pull: { reviews: { _id: reviewId } } }).exec();
}

const BookReview = mongoose.model('BookReview', bookReviewSchema);

module.exports = BookReview;