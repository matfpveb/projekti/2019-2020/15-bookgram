const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const path = require('path');
const fs = require('fs');

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    birthday: {
        type: Date
    },
    about: {
        type: String
    },
    imagePath: {
        type: String
    }
});

userSchema.index({ username: 'text' });

userSchema.statics.getAllUsers = async function() {
    const users = await this.find({}).exec();

    return users;
};


userSchema.statics.getUserById = async function(userId) {
    const user = await this.findById(userId).exec();

    if (user) {
        return Promise.resolve(user);
    } else {
        return Promise.reject();
    }
};


userSchema.statics.addUser = async function(username, password) {
    const hashedPassword = await bcrypt.hash(password, 10);

    const user = new this({
        'username': username,
        'password': hashedPassword
    });

    const savedUser = await user.save();

    return savedUser;
};

userSchema.statics.getUserByUsername = async function(username) {
    const user = await this.findOne({ username }).exec();

    return user;
};

userSchema.statics.getImagePath = async function(userId) {
    const imagePath = await this.findById(userId, '-_id imagePath').exec();

    return imagePath.imagePath;
};

userSchema.statics.changeUserInfo = async function(userId, data) {
    await this.updateOne({ _id: userId }, { $set: data }).exec();
};

userSchema.statics.changeUsername = async function(userId, newUsername) {
    await this.updateOne({ _id: userId }, { $set: { username: newUsername } }).exec();
};

userSchema.statics.changePassword = async function(userId, newPassword) {
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    await this.updateOne({ _id: userId }, { $set: { password: hashedPassword } }).exec();
};

userSchema.methods.comparePassword = async function(password) {
    return (await bcrypt.compare(password, this.password) ? true : false);
};

userSchema.statics.deleteUser = async function(userId) {
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
        let imagePath = await this.getImagePath(userId);
        if (imagePath) {
            imagePath = __dirname.replace("models", path.join('database', 'pictures', 'users', imagePath.substring(imagePath.lastIndexOf('/') + 1)));

            fs.unlink(imagePath, (err) => {
                if (err)
                    Promise.reject(err);
            });
        }

        await this.deleteOne({ _id: userId }, { session }).exec();

        await mongoose.model('BookRating').updateMany({ 'ratings.user': userId }, { $pull: { ratings: { user: userId } } }, { session }).exec();
        await mongoose.model('BookReview').updateMany({ 'reviews.user': userId }, { $pull: { reviews: { user: userId } } }, { session }).exec();
        await mongoose.model('Discussion').deleteMany({ user: userId }, { session }).exec();
        await mongoose.model('Discussion').updateMany({ 'comments.user': userId }, { $pull: { comments: { user: userId } } }, { session }).exec();
        await mongoose.model('FavouriteGenre').deleteMany({ user: userId }, { session }).exec();
        await mongoose.model('RecommendedBook').deleteMany({ user: userId }, { session }).exec();
        await mongoose.model('Shelf').deleteMany({ user: userId }, { session }).exec();

        await session.commitTransaction();
        session.endSession();
    } catch (err) {
        await session.abortTransaction();
        session.endSession();

        return Promise.reject(err);
    }
}

const User = mongoose.model('User', userSchema);

module.exports = User;