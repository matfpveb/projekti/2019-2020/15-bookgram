const mongoose = require('mongoose');

const User = require('./user'),
    Book = require('./book');


const shelfSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: User,
        required: true
    },
    books: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: Book
    }]
});

shelfSchema.statics.addNewShelf = async function(userId, name) {
    // Check if shelf with that name already exists
    const shelf = await this.findOne({ user: userId, name }).exec();

    if (shelf) {
        return Promise.reject();
    }

    const newShelf = new this({
        user: userId,
        name: name
    });

    await newShelf.save();
};

shelfSchema.statics.getShelvesByUser = async function(userId) {
    const shelves = await this.find({ user: userId }).exec();

    return shelves;
};

shelfSchema.statics.getShelvesNames = async function(userId) {
    const shelves = await this.find({ user: userId }, { 'books': 0, 'user': 0 }).exec();

    return shelves;
};

shelfSchema.statics.getShelfById = async function(shelfId) {
    const shelf = await this.findById(shelfId)
        .populate('books').exec();

    return shelf ? Promise.resolve(shelf) : Promise.reject();
}

shelfSchema.statics.addBookToShelf = async function(shelfId, bookId) {
    // Check that book
    await Book.getBookById(bookId);
    // And shelf exist
    await this.getShelfById(shelfId);

    await this.updateOne({ _id: shelfId }, { $addToSet: { books: bookId } }).exec();
};

shelfSchema.statics.removeBookFromShelf = async function(shelfId, bookId) {
    // Check that such book exists
    await Book.getBookById(bookId);

    await this.updateOne({ _id: shelfId }, { "$pull": { books: bookId } }).exec();
};

shelfSchema.statics.removeShelf = async function(shelfId) {
    await this.deleteOne({ _id: shelfId }).exec();
}

const Shelf = mongoose.model('Shelf', shelfSchema);

module.exports = Shelf;