const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const Book = require('./book'),
    Author = require('./author'),
    Genre = require('./genre'),
    User = require('./user');


const discussionSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    onId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        refPath: 'onModel'
    },
    onModel: {
        type: String,
        required: true,
        enum: ['Book', 'Author', 'Genre']
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: User,
        required: true
    },
    description: {
        type: String
    },
    comments: [{
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: User,
            required: true
        },
        body: {
            type: String,
            required: true
        },
        postedAt: {
            type: Date,
            default: Date.now
        }
    }],
    createdAt: {
        type: Date,
        default: Date.now
    }
});

discussionSchema.index({ name: 'text', description: 'text' });
discussionSchema.plugin(mongoosePaginate);


discussionSchema.statics.addNewDiscussion = async function(discussion) {
    switch (discussion.onModel) {
        case 'Author':
            // Check if the author exists
            await Author.getAuthorById(discussion.onId);
            break;
        case 'Genre':
            // Check if the genre exists
            await Genre.getGenreById(discussion.onId);
            break;
        case 'Book':
            // Check if the book exists
            await Book.getBookById(discussion.onId);
            break;
    }

    const newDiscussion = new this(discussion);

    await newDiscussion.save();
}


discussionSchema.statics.getAllUserDiscussions = async function(userId, options) {
    options.populate = 'onId user';
    options.sort = { createdAt: -1 };

    const discussions = await this.paginate({ user: userId }, options);

    return discussions;
}

discussionSchema.statics.getAllAuthorsDiscussions = async function(options) {
    options.populate = 'onId user';
    options.sort = { createdAt: -1 };
    const discussions = await this.paginate({ onModel: 'Author' }, options);

    return discussions;
}


discussionSchema.statics.searchAuthorsDiscussions = async function(options, keywords) {
    options.select = { score: { $meta: "textScore" } };
    options.sort = { score: { $meta: "textScore" } };
    options.populate = 'onId user';

    const discussions = await this.paginate({ onModel: 'Author', $text: { $search: keywords } }, options);

    return discussions;
}

discussionSchema.statics.getAllBooksDiscussions = async function(options) {
    options.populate = 'onId user';
    options.sort = { createdAt: -1 };

    const discussions = await this.paginate({ onModel: 'Book' }, options);

    return discussions;
}


discussionSchema.statics.searchBooksDiscussions = async function(options, keywords) {
    options.select = { score: { $meta: "textScore" } };
    options.sort = { score: { $meta: "textScore" } };
    options.populate = 'onId user';

    const discussions = await this.paginate({ onModel: 'Book', $text: { $search: keywords } }, options);

    return discussions;
}

discussionSchema.statics.getAllGenresDiscussions = async function(options) {
    options.populate = 'onId user';
    options.sort = { createdAt: -1 };

    const discussions = await this.paginate({ onModel: 'Genre' }, options);

    return discussions;
}


discussionSchema.statics.searchGenresDiscussions = async function(options, keywords) {
    options.select = { score: { $meta: "textScore" } };
    options.sort = { score: { $meta: "textScore" } };
    options.populate = 'onId user';

    const discussions = await this.paginate({ onModel: 'Genre', $text: { $search: keywords } }, options);

    return discussions;
}

discussionSchema.statics.getDiscussionsByAuthor = async function(authorId, options) {
    options.populate = 'onId user';
    options.sort = { createdAt: -1 };

    const discussions = await this.paginate({ onModel: 'Author', onId: authorId }, options);

    return discussions;
}

discussionSchema.statics.searchDiscussionsByAuthor = async function(authorId, options, keywords) {
    options.select = { score: { $meta: "textScore" } };
    options.sort = { score: { $meta: "textScore" } };
    options.populate = 'onId user';

    const discussions = await this.paginate({ onModel: 'Author', onId: authorId, $text: { $search: keywords } }, options);

    return discussions;
}

discussionSchema.statics.getDiscussionsByBook = async function(bookId, options) {
    options.populate = 'onId user';
    options.sort = { createdAt: -1 };

    const discussions = await this.paginate({ onModel: 'Book', onId: bookId }, options);

    return discussions;
}

discussionSchema.statics.searchDiscussionsByBook = async function(bookId, options, keywords) {
    options.select = { score: { $meta: "textScore" } };
    options.sort = { score: { $meta: "textScore" } };
    options.populate = 'onId user';

    const discussions = await this.paginate({ onModel: 'Book', onId: bookId, $text: { $search: keywords } }, options);

    return discussions;
}

discussionSchema.statics.getDiscussionById = async function(discussionId) {
    const discussion = await this.findById(discussionId)
        .populate('onId user comments.user').exec();

    return discussion ? Promise.resolve(discussion) : Promise.reject();
}


discussionSchema.statics.addNewComment = async function(discussionId, comment) {
    // Check if the discussion exists
    await this.getDiscussionById(discussionId);

    await this.updateOne({ _id: discussionId }, { "$push": { comments: comment } }).exec();
}

discussionSchema.statics.removeComment = async function(discussionId, commentId) {
    await this.updateOne({ _id: discussionId, 'comments._id': commentId }, { $pull: { comments: { _id: commentId } } }).exec();
}

discussionSchema.statics.removeDiscussion = async function(discussionId) {
    await this.deleteOne({ _id: discussionId }).exec();
}


const Discussion = mongoose.model('Discussion', discussionSchema);

module.exports = Discussion;