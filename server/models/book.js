const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const Author = require('./author'),
    Genre = require('./genre');


const bookSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: Author,
        required: true
    },
    genre: {
        type: mongoose.Schema.Types.ObjectId,
        ref: Genre,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    imagePath: {
        type: String
    }
});

bookSchema.index({ title: 'text', description: 'text' });
bookSchema.plugin(mongoosePaginate);

bookSchema.statics.getBooks = async function() {
    const books = await this.find({}, { 'title': 1 }).exec();

    return books;
}

bookSchema.statics.getAll = async function() {
    const books = await this.find({}, { description: 0, __v: 0 })
        .populate('author', 'name')
        .populate('genre', 'name').exec();

    return books;
};

bookSchema.statics.getAllBooks = async function(options) {
    const books = await this.paginate({}, options);

    return books;
}

bookSchema.statics.searchBooks = async function(options, keywords) {
    options.select = { score: { $meta: "textScore" } };
    options.sort = { score: { $meta: "textScore" } };
    options.lean = true;

    const books = await this.paginate({ $text: { $search: keywords } }, options);

    return books;
}


bookSchema.statics.getBookById = async function(bookId) {
    const book = await this.findById(bookId)
        .populate('author')
        .populate('genre')
        .exec();

    return book ? Promise.resolve(book) : Promise.reject();
}

bookSchema.statics.getBooksByGenre = async function(genreId) {
    const books = await this.find({ genre: genreId }).exec();

    return books;
};

bookSchema.statics.getBooksByAuthor = async function(authorId) {
    const books = await this.find({ author: authorId }).exec();

    return books;
}

const Book = mongoose.model('Book', bookSchema);

module.exports = Book;