const express = require('express'),
    router = express.Router();

const controller = require('../controllers/genres');


router.get('/', controller.getAllGenres);
router.get('/list', controller.getGenresList);
router.get('/discussions', controller.getAllGenresDiscussions);
router.get('/discussions/search', controller.searchGenresDiscussions);
router.get('/:genreId', controller.getGenreById);
router.get('/:genreId/books', controller.getBooksByGenre);

module.exports = router;