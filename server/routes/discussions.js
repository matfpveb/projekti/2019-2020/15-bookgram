const express = require('express'),
    router = express.Router();

const controller = require('../controllers/discussions');


router.post('/', controller.addNewDiscussion);
/*
router.get('/', controller.getAllDiscussions);
router.get('/search', controller.searchDiscussions);
*/
router.get('/authors', controller.getAllAuthorsDiscussions);
router.get('/authors/search', controller.searchAuthorsDiscussions);

router.get('/:discussionId', controller.getDiscussionById);
router.post('/:discussionId', controller.addNewComment);
router.delete('/:discussionId', controller.removeDiscussion);
router.delete('/:discussionId/:commentId', controller.removeComment);

module.exports = router;