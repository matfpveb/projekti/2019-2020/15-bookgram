const express = require('express'),
    router = express.Router();

const controller = require('../controllers/books');

router.get('/names', controller.getBooks);

router.get('/', controller.getAllBooks);
router.get('/search', controller.searchBooks);

router.get('/discussions', controller.getAllBooksDiscussions);
router.get('/discussions/search', controller.searchBooksDiscussions);

router.get('/:bookId', controller.getBookById);
router.post('/:bookId/ratings', controller.addRating);
router.get('/:bookId/ratings/average', controller.getAverageRating);
router.get('/:bookId/reviews', controller.getReviews);
router.post('/:bookId/reviews', controller.addReview);
router.delete('/:bookId/reviews/:reviewId', controller.removeReview);
router.get('/:bookId/discussions', controller.getDiscussionsByBook);
router.get('/:bookId/discussions/search', controller.searchDiscussionsByBook);

module.exports = router;