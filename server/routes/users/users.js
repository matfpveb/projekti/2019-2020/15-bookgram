const express = require('express');
const router = express.Router();

const userRoutes = require('./user');
router.use('/user', userRoutes);

const authentication = require('../../middlewares/authentication');
router.use(authentication.checkAuthenticated);

const controller = require('../../controllers/users/users');

router.get('/:userId', controller.getUserById);
router.get('/:userId/shelves', controller.getShelves);
router.get('/:userId/favouriteGenres', controller.getAllFavouriteGenres);
router.get('/:userId/recommendedBooks', controller.getRecommendBooks);

router.get('/:userId/discussions', controller.getAllUserDiscussions);
router.get('/:userId/discussions/search', controller.searchUserDiscussions);

module.exports = router;