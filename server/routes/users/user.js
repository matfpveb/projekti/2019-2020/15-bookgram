const express = require('express');
const router = express.Router();

const controller = require('../../controllers/users/user');


router.post('/register', controller.registerUser);
router.post('/login', controller.loginUser);

const authentication = require('../../middlewares/authentication');
router.use(authentication.checkAuthenticated);

router.post('/', controller.changeUserInfo);
router.delete('/', controller.deleteUser);
router.post('/username', controller.changeUsername);
router.post('/password', controller.changePassword);

router.get('/shelves/:bookId', controller.getShelvesByBook);
router.get('/ratings/:bookId', controller.getRatingByBook);
router.get('/favouriteGenres/:genreId', controller.checkIfGenreInFavourites);
router.post('/favouriteGenres', controller.addGenreToFavourites);
router.delete('/favouriteGenres', controller.removeGenreFromFavourites);

module.exports = router;