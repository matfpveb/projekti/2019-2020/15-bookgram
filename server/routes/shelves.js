const express = require('express'),
    router = express.Router();

const controller = require('../controllers/shelves');

// router.get('/', controller.getAllShelvesForUserAndBook);
router.post('/', controller.addNewShelf);
router.get('/:shelfId', controller.getShelfById);
router.post('/:shelfId', controller.addBookToShelf);
router.delete('/:shelfId', controller.removeShelf);

router.delete('/:shelfId/:bookId', controller.removeBookFromShelf);

module.exports = router;