const express = require('express'),
    router = express.Router();

const controller = require('../controllers/authors');

router.get('/names', controller.getAuthors);

router.get('/', controller.getAllAuthors);
router.get('/search', controller.searchAuthors);

router.get('/discussions/', controller.getAllAuthorsDiscussions);
router.get('/discussions/search', controller.searchAuthorsDiscussions);

router.get('/:authorId', controller.getAuthorById);
router.get('/:authorId/books', controller.getAuthorBooks);
router.get('/:authorId/discussions', controller.getDiscussionsByAuthor);
router.get('/:authorId/discussions/search', controller.searchDiscussionsByAuthor);


module.exports = router;