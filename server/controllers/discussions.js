const Discussion = require('../models/discussion');

const notFound = require('../middlewares/not-found');
const validation = require('../middlewares/validation');

module.exports.addNewDiscussion = async(req, res, next) => {
    try {
        req.body.user = req.user;

        await Discussion.addNewDiscussion(req.body);

        res.send({ message: 'New discussion created' });
    } catch (err) {
        next(err);
    }
}

module.exports.getAllAuthorsDiscussions = async(req, res, next) => {
    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    }
    try {

        const discussions = await Discussion.getAllAuthorsDiscussions(options);

        res.send(discussions);
    } catch (err) {
        next(err);
    }
}

module.exports.searchAuthorsDiscussions = async(req, res, next) => {
    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    }

    const search = req.query.search;
    try {
        const discussions = await Discussion.searchAuthorsDiscussions(options, search);

        res.send(discussions);
    } catch (err) {
        next(err);
    }
};



module.exports.getDiscussionById = [
    async(req, res, next) => {
        const discussionId = req.params.discussionId;

        try {
            const discussion = await Discussion.getDiscussionById(discussionId);

            res.send(discussion);
        } catch (err) {
            next(err);
        }
    },

    notFound
];

module.exports.addNewComment = [
    validation.commentIsEmpty,
    validation.validate,

    async(req, res, next) => {
        const discussionId = req.params.discussionId;

        const comment = {
            user: req.user.id,
            body: req.body.comment
        };
        try {
            await Discussion.addNewComment(discussionId, comment);

            res.send({ msg: 'Comment added' });
        } catch (err) {
            next(err);
        }
    },

    notFound
];


module.exports.removeComment = [
    async(req, res, next) => {
        const discussionId = req.params.discussionId;
        const commentId = req.params.commentId;

        try {
            await Discussion.removeComment(discussionId, commentId);

            res.send({ msg: 'Comment removed' });
        } catch (err) {
            next(err);
        }
    },

    notFound
];


module.exports.removeDiscussion = async(req, res, next) => {
    const discussionId = req.params.discussionId;

    try {
        await Discussion.removeDiscussion(discussionId);

        res.send({ msg: 'Discussion removed' });
    } catch (err) {
        next(err);
    }
};