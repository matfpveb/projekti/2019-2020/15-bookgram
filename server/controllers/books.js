const Book = require('../models/book');
const BookRating = require('../models/book-rating');
const BookReview = require('../models/book-review');
const Discussion = require('../models/discussion');

const notFound = require('../middlewares/not-found');
const validation = require('../middlewares/validation');

module.exports.getBooks = async(req, res, next) => {
    try {
        const books = await Book.getBooks();

        res.send(books);
    } catch (err) {
        next(err);
    }
}

module.exports.getAllBooks = async(req, res, next) => {
    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    }

    try {
        const books = await Book.getAllBooks(options);

        res.send(books);
    } catch (err) {
        next(err);
    }
}

module.exports.searchBooks = async(req, res, next) => {
    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    }

    const search = req.query.search;
    try {
        const books = await Book.searchBooks(options, search);

        res.send(books);
    } catch (err) {
        next(err);
    }
};


module.exports.getBookById = [
    async(req, res, next) => {
        const bookId = req.params.bookId;

        try {
            const book = await Book.getBookById(bookId);

            res.send(book);
        } catch (err) {
            next(err);
        }
    },

    notFound
];

module.exports.getAverageRating = async(req, res, next) => {
    const bookId = req.params.bookId;

    try {
        const rating = await BookRating.getAverageRating(bookId);

        res.send(rating);
    } catch (err) {
        next(err);
    }
};

module.exports.addRating = [
    validation.ratingIsEmpty,
    validation.validate,

    async(req, res, next) => {
        const bookId = req.params.bookId;
        const userId = req.user.id;

        const rating = req.body.rating;
        try {
            await BookRating.addRating(bookId, userId, rating);

            res.send({ message: 'Rating added' });
        } catch (err) {
            next(err);
        }
    }
]

module.exports.getReviews = async(req, res, next) => {
    const bookId = req.params.bookId;

    try {
        const reviews = await BookReview.getReviews(bookId);

        res.send({ reviews: reviews ? reviews.reviews : [] });
    } catch (err) {
        next(err);
    }
};

module.exports.addReview = [
    validation.reviewIsEmpty,
    validation.validate,

    async(req, res, next) => {
        const bookId = req.params.bookId;
        const userId = req.user.id;

        const review = req.body.review;
        try {
            await BookReview.addReview(bookId, userId, review);

            res.send({ message: 'Rating added' });
        } catch (err) {
            next(err);
        }
    }
]

module.exports.removeReview = [
    async(req, res, next) => {
        const bookId = req.params.bookId;
        const reviewId = req.params.reviewId;

        try {
            await BookReview.removeReview(bookId, reviewId);

            res.send({ msg: 'Review removed' });
        } catch (err) {
            next(err);
        }
    },

    notFound
];

module.exports.getAllBooksDiscussions = async(req, res, next) => {
    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    }
    try {
        const discussions = await Discussion.getAllBooksDiscussions(options);

        res.send(discussions);
    } catch (err) {
        next(err);
    }
}

module.exports.searchBooksDiscussions = async(req, res, next) => {
    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    }

    const search = req.query.search;
    try {
        const discussions = await Discussion.searchBooksDiscussions(options, search);

        res.send(discussions);
    } catch (err) {
        next(err);
    }
};


module.exports.getDiscussionsByBook = async(req, res, next) => {
    const bookId = req.params.bookId;

    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    };
    try {
        const discussions = await Discussion.getDiscussionsByBook(bookId, options);

        res.send(discussions);
    } catch (err) {
        next(err);
    }
}

module.exports.searchDiscussionsByBook = async(req, res, next) => {
    const bookId = req.params.bookId;

    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    }

    const search = req.query.search;
    try {
        const discussions = await Discussion.searchDiscussionsByBook(bookId, options, search);

        res.send(discussions);
    } catch (err) {
        next(err);
    }
};