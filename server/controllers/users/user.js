const jwt = require('jsonwebtoken');
const secret = require('../../configurations/secret');
const fs = require('fs');
const path = require('path');
const upload = require('../../middlewares/multer').upload;


const User = require('../../models/user');
const Shelf = require('../../models/shelf');
const BookRating = require('../../models/book-rating');
const FavouriteGenre = require('../../models/favourite-genre');

const usernameExists = require('../../middlewares/username-exists');
const invalidData = require('../../middlewares/invalid-data');
const validation = require('../../middlewares/validation');

module.exports.registerUser = [
    validation.usernameIsEmpty,
    validation.passwordIsEmpty,
    validation.validate,

    async(req, res, next) => {
        const username = req.body.username;
        const password = req.body.password;

        try {
            let user = await User.getUserByUsername(username);

            if (!user) {
                user = await User.addUser(username, password);

                // Automatically login in user 
                // If one is created
                const token = jwt.sign({ id: user._id, username: user.username },
                    secret, { expiresIn: '1h' });

                res.status(201).send({ token });
            } else {
                next();
            }
        } catch (error) {
            next(error);
        }
    },

    usernameExists
];


module.exports.loginUser = [
    validation.usernameIsEmpty,
    validation.passwordIsEmpty,
    validation.validate,

    async(req, res, next) => {
        const username = req.body.username;
        const password = req.body.password;

        try {
            const user = await User.getUserByUsername(username);

            if (user) {
                if (await user.comparePassword(password)) {
                    const token = jwt.sign({ id: user._id, username: user.username },
                        secret, { expiresIn: '1h' });

                    res.send({ token });
                } else {
                    next();
                }
            } else {
                next();
            }
        } catch (err) {
            next(err);
        }
    },

    invalidData
];

module.exports.getUserById = async(req, res, next) => {
    const userId = req.params.userId;

    try {
        const user = await User.getUserById(userId);

        res.send(user);
    } catch (err) {
        next(err);
    }
};

module.exports.changeUserInfo = [
    upload.single('image'),

    async(req, res, next) => {
        const body = req.body;

        try {
            // If file exists
            if (req.file) {
                // Get its path for db
                // http://localhost:3000/pictures/users/imageName
                const newImagePath = req.protocol + "://" + req.get("host") + '/pictures/users/' + req.file.filename;

                const oldImagePath = await User.getImagePath(req.user.id);

                // If we had a file with different extention
                // Delete it
                if (oldImagePath && oldImagePath !== newImagePath) {
                    const serverOldImgPath = __dirname.replace("controllers/users", path.join('database', 'pictures', 'users', oldImagePath.substring(oldImagePath.lastIndexOf('/') + 1)));

                    fs.unlink(serverOldImgPath, (err) => {
                        if (err)
                            next(err);
                    });
                }

                body.imagePath = newImagePath;

                delete req.file;
            }

            await User.changeUserInfo(req.user.id, body);

            res.send({ msg: 'User information saved.' });
        } catch (err) {
            next(err);
        }
    }
];

module.exports.changeUsername = [
    validation.usernameIsEmpty,
    validation.validate,

    async(req, res, next) => {
        const username = req.body.username;
        const currentUser = req.user;

        try {
            let user = await User.getUserByUsername(username);

            if (!user) {
                if (await currentUser.comparePassword(req.body.password)) {

                    await User.changeUsername(currentUser.id, username);

                    res.send({ msg: 'Username changed!' });
                } else {
                    const error = new Error('INVALID_DATA');
                    error.status = 400;

                    next(error);
                }
            } else {
                next();
            }
        } catch (error) {
            next(error);
        }
    },

    usernameExists
];


module.exports.changePassword = [
    validation.passwordIsEmpty,
    validation.oldPasswordIsEmpty,
    validation.validate,

    async(req, res, next) => {
        const password = req.body.password;
        const oldPassword = req.body.oldPassword;
        const currentUser = req.user;

        try {
            if (await currentUser.comparePassword(oldPassword)) {
                await User.changePassword(currentUser.id, password);

                res.send({ msg: 'Password changed!' });
            } else {
                next();
            }
        } catch (error) {
            next(error);
        }
    },

    invalidData
];


module.exports.getShelvesByBook = async(req, res, next) => {
    const userId = req.user.id;

    const bookId = req.params.bookId;
    try {
        let shelves = await Shelf.getShelvesByUser(userId);

        shelves = shelves.map((shelf) => { return { _id: shelf._id, name: shelf.name, exists: shelf.books.includes(bookId) } });

        res.send(shelves);
    } catch (err) {
        next(err);
    }
};

module.exports.getRatingByBook = async(req, res, next) => {
    const userId = req.user.id;
    const bookId = req.params.bookId;

    try {
        const rating = await BookRating.getUserRating(bookId, userId);

        res.send(rating);
    } catch (err) {
        next(err);
    }
};

module.exports.checkIfGenreInFavourites = async(req, res, next) => {
    const genreId = req.params.genreId;
    const userId = req.user.id;

    try {
        const exists = await FavouriteGenre.checkIfInFavourites(userId, genreId);

        res.send(exists);
    } catch (err) {
        next(err);
    }
};

module.exports.addGenreToFavourites = [
    validation.genreIdIsEmpty,
    validation.validate,

    async(req, res, next) => {
        const userId = req.user.id;
        const genreId = req.body.genreId;

        try {
            await FavouriteGenre.addToFavouriteGenres(userId, genreId);

            res.send({ msg: 'Ok' });
        } catch (err) {
            next(err);
        }

    }
]


module.exports.removeGenreFromFavourites = [
    validation.genreIdIsEmpty,
    validation.validate,

    async(req, res, next) => {
        const genreId = req.body.genreId;
        const userId = req.user.id;

        try {
            await FavouriteGenre.removeFromFavouriteGenres(userId, genreId);

            res.send({ msg: 'Genre removed from favourites' });
        } catch (err) {
            next(err);
        }
    }
]

module.exports.deleteUser = [
    validation.passwordIsEmpty,
    validation.validate,

    async(req, res, next) => {
        const user = req.user;
        const password = req.body.password;

        try {
            if (await user.comparePassword(password)) {

                await User.deleteUser(user.id);

                res.send({ msg: 'User deleted!' });
            } else {
                next();
            }
        } catch (err) {
            next(err);
        }
    },
    invalidData
]