const User = require('../../models/user');
const Shelf = require('../../models/shelf');
const Discussion = require('../../models/discussion');
const FavouriteGenre = require('../../models/favourite-genre');
const RecommendedBook = require('../../models/recommended-book');
const { re } = require('mathjs');

module.exports.getUserById = async(req, res, next) => {
    const userId = req.params.userId;

    try {
        const user = await User.getUserById(userId);

        res.send(user);
    } catch (err) {
        next(err);
    }
};

module.exports.getShelves = async(req, res, next) => {
    const userId = req.params.userId;

    try {
        const shelves = await Shelf.getShelvesNames(userId);

        res.send(shelves);
    } catch (err) {
        next(err);
    }
}


module.exports.getAllUserDiscussions = async(req, res, next) => {
    const userId = req.params.userId;

    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    }

    try {
        const discussions = await Discussion.getAllUserDiscussions(userId, options);

        res.send(discussions);
    } catch (err) {
        next(err);
    }
}

module.exports.searchUserDiscussions = async(req, res, next) => {
    const userId = req.params.userId;

    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    }

    const search = req.query.search;
    try {
        const discussions = await Discussion.searchUserDiscussions(options, search);

        res.send(discussions);
    } catch (err) {
        next(err);
    }
};

module.exports.getAllFavouriteGenres = async(req, res, next) => {
    const userId = req.params.userId;

    try {
        const favouriteGenres = await FavouriteGenre.getFavouriteGenresByUser(userId);

        res.send({ favouriteGenres: favouriteGenres ? favouriteGenres.genres : [] });
    } catch (err) {
        next(err);
    }
}

module.exports.getRecommendBooks = async(req, res, next) => {
    const userId = req.params.userId;

    try {
        const recommendedBooks = await RecommendedBook.getBooks(userId);

        res.send(recommendedBooks[0]);
    } catch (err) {
        next(err);
    }
}