const Shelf = require('../models/shelf');

const unprocessableEntity = require('../middlewares/unprocessable-entity');
const validation = require('../middlewares/validation');

module.exports.addNewShelf = [
    validation.nameIsEmpty,
    validation.validate,

    async(req, res, next) => {
        const userId = req.user.id;

        const name = req.body.name;
        try {
            await Shelf.addNewShelf(userId, name);

            res.send({ msg: 'New shelf added' });
        } catch (err) {
            next(err);
        }
    },

    unprocessableEntity
];

module.exports.addBookToShelf = [
    validation.bookIdIsEmpty,
    validation.validate,

    async(req, res, next) => {
        const shelfId = req.params.shelfId;
        const bookId = req.body.bookId;

        try {
            await Shelf.addBookToShelf(shelfId, bookId);

            res.send({ msg: 'Ok' });
        } catch (err) {
            next(err);
        }
    }
]

module.exports.removeBookFromShelf = async(req, res, next) => {
    const shelfId = req.params.shelfId;
    const bookId = req.params.bookId;

    try {
        await Shelf.removeBookFromShelf(shelfId, bookId);

        res.send({ msg: 'Ok' });
    } catch (err) {
        next(err);
    }
};

module.exports.getShelfById = async(req, res, next) => {
    const shelfId = req.params.shelfId;

    try {
        const shelf = await Shelf.getShelfById(shelfId);

        res.send(shelf);
    } catch (err) {
        next(err);
    }
}

module.exports.removeShelf = async(req, res, next) => {
    const shelfId = req.params.shelfId;

    try {
        await Shelf.removeShelf(shelfId);

        res.send({ msg: 'Shelf removed' });
    } catch (err) {
        next(err);
    }
}