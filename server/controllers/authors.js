const Author = require('../models/author');
const Book = require('../models/book');
const Discussion = require('../models/discussion');

const notFound = require('../middlewares/not-found');

module.exports.getAuthors = async(req, res, next) => {
    try {
        const authors = await Author.getAuthors();

        res.send(authors);
    } catch (err) {
        next(err);
    }
}

module.exports.getAllAuthors = async(req, res, next) => {
    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    }

    try {
        const authors = await Author.getAllAuthors(options);

        res.send(authors);
    } catch (err) {
        next(err);
    }
}

module.exports.searchAuthors = async(req, res, next) => {
    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    }

    const search = req.query.search;
    try {
        const authors = await Author.searchAuthors(options, search);

        res.send(authors);
    } catch (err) {
        next(err);
    }
};


module.exports.getAuthorById = [
    async(req, res, next) => {
        const authorId = req.params.authorId;

        try {
            const author = await Author.getAuthorById(authorId);

            res.send(author);
        } catch (err) {
            next(err);
        }
    },

    notFound
];


module.exports.getAuthorBooks = [
    async(req, res, next) => {
        const authorId = req.params.authorId;

        try {
            const books = await Book.getBooksByAuthor(authorId);

            res.send(books);
        } catch (err) {
            next(err);
        }
    },

    notFound
];


module.exports.getAllAuthorsDiscussions = async(req, res, next) => {
    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    }
    try {

        const discussions = await Discussion.getAllAuthorsDiscussions(options);

        res.send(discussions);
    } catch (err) {
        next(err);
    }
}

module.exports.searchAuthorsDiscussions = async(req, res, next) => {
    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    }

    const search = req.query.search;
    try {
        const discussions = await Discussion.searchAuthorsDiscussions(options, search);

        res.send(discussions);
    } catch (err) {
        next(err);
    }
};


module.exports.getDiscussionsByAuthor = async(req, res, next) => {
    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    };

    const authorId = req.params.authorId;
    try {
        const discussions = await Discussion.getDiscussionsByAuthor(authorId, options);

        res.send(discussions);
    } catch (err) {
        next(err);
    }
}

module.exports.searchDiscussionsByAuthor = async(req, res, next) => {
    const authorId = req.params.authorId;

    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    }

    const search = req.query.search;
    try {
        const discussions = await Discussion.searchDiscussionsByAuthor(authorId, options, search);

        res.send(discussions);
    } catch (err) {
        next(err);
    }
};