const Genre = require('../models/genre');
const Book = require('../models/book');
const Discussion = require('../models/discussion');

const notFound = require('../middlewares/not-found');

module.exports.getGenresList = async(req, res, next) => {
    try {
        const genres = await Genre.getGenresList();

        res.send(genres);
    } catch (err) {
        next(err);
    }
};


module.exports.getAllGenres = async(req, res, next) => {
    try {
        const genres = await Genre.getAllGenres();

        res.send(genres);
    } catch (err) {
        next(err);
    }
}

module.exports.getGenreById = [
    async(req, res, next) => {
        const genreId = req.params.genreId;
        try {
            const genre = await Genre.getGenreById(genreId);

            res.send(genre);
        } catch (err) {
            next(err);
        }
    },

    notFound
];

module.exports.getBooksByGenre = async(req, res, next) => {
    const genreId = req.params.genreId;

    try {
        const books = await Book.getBooksByGenre(genreId);

        res.send(books);
    } catch (err) {
        next(err);
    }
}


module.exports.getAllGenresDiscussions = async(req, res, next) => {
    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    }
    try {
        const discussions = await Discussion.getAllGenresDiscussions(options);

        res.send(discussions);
    } catch (err) {
        next(err);
    }
}

module.exports.searchGenresDiscussions = async(req, res, next) => {
    const options = {
        page: +req.query.page,
        limit: +req.query.limit
    }

    const search = req.query.search;
    try {
        const discussions = await Discussion.searchGenresDiscussions(options, search);

        res.send(discussions);
    } catch (err) {
        next(err);
    }
};