const emailExists = (req, res, next) => {
    const error = new Error('USERNAME_EXISTS');
    error.status = 400;

    next(error);
};

module.exports = emailExists;