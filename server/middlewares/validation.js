const { check, oneOf, validationResult } = require('express-validator'),
    bcrypt = require('bcrypt');

const User = require('../models/user');

module.exports.usernameIsEmpty = check('username', 'Username must not be empty').trim().notEmpty();

module.exports.passwordIsEmpty = check('password', 'Password must not be empty').trim().notEmpty();

module.exports.oldPasswordIsEmpty = check('oldPassword', 'Old password must be provided').trim().notEmpty();

module.exports.genreIdIsEmpty = check('genreId', 'Genre id must not be empty').trim().notEmpty();

module.exports.ratingIsEmpty = check('rating', 'Rating must not be empty').trim().notEmpty();

module.exports.reviewIsEmpty = check('review', 'Review must not be empty').trim().notEmpty();

module.exports.commentIsEmpty = check('comment', 'Comment must not be empty').trim().notEmpty();

module.exports.nameIsEmpty = check('name', 'Name must not be empty').trim().notEmpty();

module.exports.bookIdIsEmpty = check('bookId', 'Book id must not be empty').trim().notEmpty();

module.exports.validate = (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        const error = new Error();
        error.status = 422;
        error.message = errors.array().map(err => err.msg);

        next(error);
    } else {
        next();
    }
};