const unprocessableEntity = (req, res, next) => {
    const error = new Error('UNPROCESABLE_ENTITY');
    error.status = 422;

    next(error);
};

module.exports = unprocessableEntity;