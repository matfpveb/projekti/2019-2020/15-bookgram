const multer = require('multer');
const path = require('path');

const User = require('../models/user');

/*
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const loc = __dirname.replace("middlewares", path.join('database', 'pictures', 'users'));

        cb(null, loc);
    },

    filename: async(req, file, cb) => {
        const ext = file.originalname.substring(file.originalname.lastIndexOf('.')) || '';

        cb(null, req.user.id + ext);
    }
});


module.exports.upload = multer({ storage: storage });*/


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const loc = __dirname.replace("middlewares", path.join('database', 'pictures', 'users'));

        cb(null, loc);
    },

    filename: async(req, file, cb) => {
        const ext = file.originalname.substring(file.originalname.lastIndexOf('.')) || '';

        cb(null, req.user.id + ext);
    }
});


module.exports.upload = multer({ storage: storage });