# Project 15-bookgram

Pravimo licnu biblioteku u kome prijavljeni korisnici stavljaju procitanje knjige i zeljene zanrove, ostavljaju utiske i komentare za knjige i dobijaju preporuke za knjige. Takodje mogu da pogledaju i knjige koje su drugi korisnici procitali, njihove komentare i preporuke.

## Pokretanje

1. Potrebno je replicirati bazu: 
    1. Zaustavljanje monga: ```sudo killall -9 mongod```
    2. Instaliranje: ```npm install run-rs -g```
    3. Pokretanje: ```run-rs -v 4.0.0 --shell```
2. Učitavanje baze: Pozicionirati se u direktorijum ```./server/database``` i u njemu izvršiti komandu ```mongorestore -d bookgram bookgram```
3. Pozicionirati se u direktorijum ```./server``` i u njemu izvršiti komande ```npm install``` i  ```npm start```
4. Pozicionirati se u direktorijum ```./client``` i u njemu izvršiti komande ```npm install``` i  ```ng serve```
5. Pokrenuti aplikaciju na ```http://localhost:4200```

## Developers

- [Teodora Heldrih, 99/15](https://gitlab.com/teaheld)
- [Jana Jovičić, 1097/19](https://gitlab.com/jana-jovicic)
- [Nevena Mesar, 107/2015](https://gitlab.com/nevena-m)
- [Danilo Vučković, 87/15](https://gitlab.com/danilo1996)
