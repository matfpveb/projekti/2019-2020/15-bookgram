import { Observable, throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

export abstract class HttpErrorHandler {
  constructor(protected router: Router) {}

  protected handleError() {
    return (error: HttpErrorResponse): Observable<never> => {
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        if (error.error) {
          switch (error.error.errorMessage) {
            case 'USERNAME_EXISTS':
              return throwError('Username already exists');
              break;
            case 'INVALID_DATA':
              return throwError('Invalid username or password');
              break;
            case 'UNPROCESABLE_ENTITY':
              return throwError('UNPROCESABLE_ENTITY');
          }
        }

        this.router.navigate([
          '/error',
          { message: error.error.errorMessage, statusCode: error.status },
        ]);
      }
      // return an observable with a user-facing error message
      return throwError('Something bad happened; please try again later.');
    };
  }
}
