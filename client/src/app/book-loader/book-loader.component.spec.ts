import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookLoaderComponent } from './book-loader.component';

describe('BookLoaderComponent', () => {
  let component: BookLoaderComponent;
  let fixture: ComponentFixture<BookLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
