import { Subscription } from 'rxjs';
import { DiscussionsService } from 'src/app/discussions/discussions.service';
import { MatDialog } from '@angular/material/dialog';
import { GenresService } from './genres.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NewDiscussionDialogComponent } from '../discussions/new-discussion-dialog/new-discussion-dialog.component';
import { Genre } from './genres.model';

@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.css']
})
export class GenresComponent implements OnInit, OnDestroy {
  public showGenresList: boolean;
  public genresList: Genre[];
  private activeSubs: Subscription[];

  constructor(private genresService: GenresService,
              private dialog: MatDialog,
              private discussionsService: DiscussionsService) {
    this.showGenresList = false;
    this.genresList = [];
    this.activeSubs = [];

    this.getGenresList();
  }

  ngOnInit(): void {
  }

  onShowGenresList() {
    this.showGenresList = !this.showGenresList;
  }

  getGenresList() {
    const sub = this.genresService.getGenresList()
      .subscribe((res: Genre[]) => {
        this.genresList = res;
      });

    this.activeSubs.push(sub);
  }

  onOpenDialog() {
    const dialogRef = this.dialog.open(NewDiscussionDialogComponent,
      {
        width: '520px',
      data: {
        type: 'Genre'
      }
    });

    const sub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.newDiscussion) {
          this.discussionsService.onNewDiscussionAdded();
        }
      }
    });

    this.activeSubs.push(sub);
  }

  ngOnDestroy(): void {
    this.dialog.closeAll();

    this.activeSubs.forEach(sub => sub.unsubscribe());
  }
}
