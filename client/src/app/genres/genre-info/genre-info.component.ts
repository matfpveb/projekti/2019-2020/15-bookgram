import { GenresService } from './../genres.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { map, switchMap } from 'rxjs/operators';
import { UserService } from 'src/app/users/user/user.service';
import { Genre } from '../genres.model';
import { Book } from 'src/app/books/books.model';

@Component({
  selector: 'app-genre-info',
  templateUrl: './genre-info.component.html',
  styleUrls: ['./genre-info.component.css']
})
export class GenreInfoComponent implements OnInit, OnDestroy {
  public isLoading: boolean;
  public genre: Genre;
  public books: Book[];
  public genreInFavourites: boolean;
  private activeSubs: Subscription[];

  constructor(private route: ActivatedRoute,
              private genresService: GenresService,
              private userService: UserService) {
    this.isLoading = true;
    this.genreInFavourites = false;
    this.activeSubs = [];

    this.getGenreById();
  }

  ngOnInit(): void { }

  getGenreById() {
    const sub = this.route.paramMap
    .pipe(
      map((params) => params.get('genreId')),
      switchMap(genreId =>
        this.genresService.getGenreById(genreId)
      )
    )
    .subscribe((res: Genre) => {
      this.genre = res;
      this.checkIfGenreInFavourites();
      this.getBooksByGenre();
    });

    this.activeSubs.push(sub);
  }

  getBooksByGenre() {
    const sub = this.genresService.getBooksByGenre(this.genre._id)
      .subscribe((res: Book[]) => {
        this.books = res;
        this.isLoading = false;
      });

    this.activeSubs.push(sub);
  }

  checkIfGenreInFavourites() {
    const sub = this.userService.checkIfGenreInFavourites(this.genre._id)
      .subscribe((res: boolean) => {
        this.genreInFavourites = res;
      });

    this.activeSubs.push(sub);
  }

  addGenreToFavourites() {
    const body = {
      genreId: this.genre._id
    };

    const sub = this.userService.addGenreToFavourites(body)
      .subscribe((res) => {
        this.genreInFavourites = true;
      });

    this.activeSubs.push(sub);
  }

  removeGenreFromFavourites() {
    const body = {
      genreId: this.genre._id
    };

    const sub = this.userService.removeGenreFromFavourites(body)
      .subscribe((res) => {
        this.genreInFavourites = false;
      });

    this.activeSubs.push(sub);
  }

  ngOnDestroy(): void {
    this.activeSubs.forEach((sub) =>
      sub.unsubscribe());
  }
}
