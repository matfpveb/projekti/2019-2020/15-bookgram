import { Subscription } from 'rxjs';
import { GenresService } from './../genres.service';
import { Component, OnInit } from '@angular/core';
import { Genre } from '../genres.model';

@Component({
  selector: 'app-list-all-genres',
  templateUrl: './list-all-genres.component.html',
  styleUrls: ['./list-all-genres.component.css']
})
export class ListAllGenresComponent implements OnInit {
  public genresList: Genre[];
  private activeSubs: Subscription[];

  constructor(private genresService: GenresService) {
    this.genresList = [];
    this.activeSubs = [];

    this.getGenres();
  }

  getGenres() {
    const sub = this.genresService.getGenresList()
      .subscribe((res: Genre[]) => {
        this.genresList = res;
      });

    this.activeSubs.push(sub);
  }

  ngOnInit(): void {
  }

}
