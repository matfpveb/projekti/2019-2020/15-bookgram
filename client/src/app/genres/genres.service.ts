import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpErrorHandler } from '../utils/http-error-handler.model';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GenresService extends HttpErrorHandler {
  public readonly genresUrl = 'http://localhost:3000/genres/';

  constructor(router: Router,
              private http: HttpClient) {
    super(router);
  }

  getGenresList() {
    return this.http.get(this.genresUrl)
      .pipe(catchError(super.handleError()));
  }

  getGenreById(genreId: string) {
    return this.http.get(this.genresUrl + `${genreId}`)
      .pipe(catchError(super.handleError()));
  }

  getBooksByGenre(genreId: string) {
    return this.http.get(this.genresUrl + `${genreId}/books`)
      .pipe(catchError(super.handleError()));
  }

  getAllGenresDiscussions(options) {
    return this.http.get(this.genresUrl + `discussions`, { params: options })
      .pipe(catchError(super.handleError()));
  }

  searchGenresDiscussions(options) {
    return this.http.get(this.genresUrl + `discussions/search`, { params: options })
      .pipe(catchError(super.handleError()));
  }
}
