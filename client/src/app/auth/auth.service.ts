import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { catchError, tap } from 'rxjs/operators';
import { throwError, BehaviorSubject, Subject } from 'rxjs';
import { HttpErrorHandler } from '../utils/http-error-handler.model';

import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends HttpErrorHandler {
  private readonly usersUrl = 'http://localhost:3000/users/user/';
  // tslint:disable-next-line: variable-name
  private _authStatusListener = new Subject<boolean>();

  constructor(private http: HttpClient,
              router: Router) {
    super(router);
  }

  getTokenAndValidity() {
    const token = localStorage.getItem('token');

    if (token) {
      const decodedToken = jwt_decode(token);

      // Our token expired
      // We will have to log back in
      if (Date.now() > decodedToken.exp * 1000) {
        this.logout();

        return { token, validity: false };
      }

      // Our token is valid
      return { token, validity: true };
    }

    // We don't have a token
    return false;
  }

  register(data: { username: string, password: string }) {
    return this.http.post<any>(this.usersUrl + 'register', data)
      .pipe(catchError(super.handleError()),
            tap(res => {
              this.authenticate(res.token);
            }));
  }

  login(data: { username: string, password: string }) {
    return this.http.post<any>(this.usersUrl + 'login', data)
      .pipe(catchError(super.handleError()),
            tap(res => {
              this.authenticate(res.token);
            }));
  }

  logout() {
    localStorage.removeItem('token');

    this._authStatusListener.next(false);

    this.router.navigate(['/login']);
  }

  private authenticate(token: string) {
    localStorage.setItem('token', JSON.stringify(token));

    this._authStatusListener.next(true);
  }

  get authStatusListener() {
    return this._authStatusListener.asObservable();
  }
}
