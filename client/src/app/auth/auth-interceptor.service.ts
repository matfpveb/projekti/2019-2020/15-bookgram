import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, EMPTY } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>,
            next: HttpHandler
            ): Observable<HttpEvent<any>> {
    let token = this.authService.getTokenAndValidity();

    if (token) {
      // If we have a valid token
      // Send request to server
      if (token.validity) {
        token = JSON.parse(token.token);

        const cloned = req.clone({
          headers: req.headers.set('Authorization',
              'Bearer ' + token)
        });

        return next.handle(cloned);
      }

      // If we don't have a valid token
      // Token expired
      // We don't want to send a request to server
      return EMPTY;
    }
    else {
        return next.handle(req);
    }
  }
}
