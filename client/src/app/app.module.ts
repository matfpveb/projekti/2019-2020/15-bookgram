import { AuthInterceptorService } from './auth/auth-interceptor.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ContenteditableModule } from '@ng-stack/contenteditable';
import { FileControlModule } from 'ng-validator';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { FooterComponent } from './footer/footer.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { BookLoaderComponent } from './book-loader/book-loader.component';
import { AuthorsListComponent } from './authors/authors-list/authors-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NewDiscussionDialogComponent } from './discussions/new-discussion-dialog/new-discussion-dialog.component';
import { AuthorsComponent } from './authors/authors.component';
import { AuthorsItemComponent } from './authors/authors-list/authors-item/authors-item.component';
import { AuthorsInfoComponent } from './authors/authors-info/authors-info.component';
import { AuthorsBooksComponent } from './authors/authors-info/authors-books/authors-books.component';
import { BooksComponent } from './books/books.component';
import { AuthorsDiscussionsComponent } from './authors/authors-discussions/authors-discussions.component';
import { SearchComponent } from './search/search.component';
import { ErrorPageComponent } from './routing/error-page/error-page.component';
import { DiscussionsInfoDialogComponent } from './discussions/discussions-info-dialog/discussions-info-dialog.component';
import { BooksListComponent } from './books/books-list/books-list.component';
import { BooksItemComponent } from './books/books-list/books-item/books-item.component';
import { BooksInfoComponent } from './books/books-info/books-info.component';
import { NewShelfDialogComponent } from './shelves/new-shelf-dialog/new-shelf-dialog.component';
import { ShelvesListDialogComponent } from './shelves/shelves-list-dialog/shelves-list-dialog.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BookRatingComponent } from './books/books-info/book-rating/book-rating.component';
import { BookReviewsListDialogComponent } from './books/books-info/book-reviews-list-dialog/book-reviews-list-dialog.component';
import { BookReviewsItemComponent } from './books/books-info/book-reviews-list-dialog/book-reviews-item/book-reviews-item.component';
import { UserInfoComponent } from './users/user-info/user-info.component';
import { UserShelvesComponent } from './users/user-info/user-shelves/user-shelves.component';
import { ShelfComponent } from './users/user-info/user-shelves/shelf/shelf.component';
import { BooksListImageItemComponent } from './books/books-list-image-item/books-list-image-item.component';
import { DiscussionsListItemComponent } from './discussions/discussions-list-item/discussions-list-item.component';
import { GenresComponent } from './genres/genres.component';
import { GenreInfoComponent } from './genres/genre-info/genre-info.component';
import { UserInfoUnchangableComponent } from './users/user-info/user-info-unchangable/user-info-unchangable.component';
import { UserInfoChangeComponent } from './users/user-info/user-info-change/user-info-change.component';
import { AuthorDiscussionsComponent } from './authors/authors-info/author-discussions/author-discussions.component';
import { DiscussionsListSmallerItemComponent } from './discussions/discussions-list-smaller-item/discussions-list-smaller-item.component';
import { BooksDiscussionsComponent } from './books/books-discussions/books-discussions.component';
import { BookDiscussionsComponent } from './books/books-info/book-discussions/book-discussions.component';
import { GenresDiscussionsComponent } from './genres/genres-discussions/genres-discussions.component';
import { UsernameChangeDialogComponent } from './users/user-info/user-info-change/username-change-dialog/username-change-dialog.component';
import { PasswordChangeDialogComponent } from './users/user-info/user-info-change/password-change-dialog/password-change-dialog.component';
import { NoShelvesComponent } from './users/user-info/user-shelves/no-shelves/no-shelves.component';
import { NoBooksComponent } from './users/user-info/user-shelves/shelf/no-books/no-books.component';
import { FavouriteGenresComponent } from './users/user-info/favourite-genres/favourite-genres.component';
import { RecommendedBooksComponent } from './users/user-info/recommended-books/recommended-books.component';
import { DeleteProfileComponent } from './users/user-info/user-info-change/delete-profile/delete-profile.component';
import { OrderByTimePipe } from './books/books-info/book-reviews-list-dialog/order-by-time.pipe';
import { NoFavouriteGenresComponent } from './users/user-info/favourite-genres/no-favourite-genres/no-favourite-genres.component';
import { ListAllGenresComponent } from './genres/list-all-genres/list-all-genres.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    FooterComponent,
    RegisterComponent,
    HomeComponent,
    LoginComponent,
    BookLoaderComponent,
    AuthorsListComponent,
    NewDiscussionDialogComponent,
    AuthorsComponent,
    AuthorsItemComponent,
    AuthorsInfoComponent,
    AuthorsBooksComponent,
    BooksComponent,
    AuthorsDiscussionsComponent,
    SearchComponent,
    ErrorPageComponent,
    DiscussionsInfoDialogComponent,
    BooksListComponent,
    BooksItemComponent,
    BooksInfoComponent,
    NewShelfDialogComponent,
    ShelvesListDialogComponent,
    BookRatingComponent,
    BookReviewsListDialogComponent,
    BookReviewsItemComponent,
    UserInfoComponent,
    UserShelvesComponent,
    ShelfComponent,
    BooksListImageItemComponent,
    DiscussionsListItemComponent,
    GenresComponent,
    GenreInfoComponent,
    UserInfoUnchangableComponent,
    UserInfoChangeComponent,
    AuthorDiscussionsComponent,
    DiscussionsListSmallerItemComponent,
    BooksDiscussionsComponent,
    BookDiscussionsComponent,
    GenresDiscussionsComponent,
    UsernameChangeDialogComponent,
    PasswordChangeDialogComponent,
    NoShelvesComponent,
    NoBooksComponent,
    FavouriteGenresComponent,
    RecommendedBooksComponent,
    DeleteProfileComponent,
    OrderByTimePipe,
    NoFavouriteGenresComponent,
    ListAllGenresComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatPaginatorModule,
    MatCheckboxModule,
    NgbModule,
    ContenteditableModule,
    FileControlModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
