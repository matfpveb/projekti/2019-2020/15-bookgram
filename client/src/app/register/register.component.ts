import { UserService } from 'src/app/users/user/user.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdvancedValidators, CommonValidators } from 'ng-validator';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {
  public error: string;
  public isLoading = false;
  public form: FormGroup;
  private activeSubs: Subscription[];

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private userService: UserService) {
    this.activeSubs = [];

    this.form = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      password2: ['', [Validators.required, AdvancedValidators.equalsToField('password', 'listener')]]
    });

    const sub = this.username.valueChanges
      .subscribe((data) => {
        if (this.error) {
          this.error = '';
        }
       });

    this.activeSubs.push(sub);
  }

  ngOnInit(): void { }

  onSubmit() {
    const data = {
      username: this.username.value,
      password: this.password.value
    };

    this.isLoading = true;
    const sub = this.authService.register(data)
        .subscribe( res => {
          this.isLoading = false;

          const userId = this.userService.getUserId();
          this.router.navigate(['/users', userId]);
        }, error => {
          this.isLoading = false;

          this.error = error;
        });

    this.activeSubs.push(sub);
  }

  public get username() {
    return this.form.get('username');
  }
  public get password() {
    return this.form.get('password');
  }
  public get password2() {
    return this.form.get('password2');
  }

  ngOnDestroy() {
    AdvancedValidators.destroyListener('listener');

    this.activeSubs.forEach((sub) => sub.unsubscribe());
  }
}
