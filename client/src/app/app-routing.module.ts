import { ListAllGenresComponent } from './genres/list-all-genres/list-all-genres.component';
import { RecommendedBooksComponent } from './users/user-info/recommended-books/recommended-books.component';
import { FavouriteGenresComponent } from './users/user-info/favourite-genres/favourite-genres.component';
import { NoShelvesComponent } from './users/user-info/user-shelves/no-shelves/no-shelves.component';
import { GenresDiscussionsComponent } from './genres/genres-discussions/genres-discussions.component';
import { BookDiscussionsComponent } from './books/books-info/book-discussions/book-discussions.component';
import { BooksDiscussionsComponent } from './books/books-discussions/books-discussions.component';
import { AuthorDiscussionsComponent } from './authors/authors-info/author-discussions/author-discussions.component';
import { GenreInfoComponent } from './genres/genre-info/genre-info.component';
import { ShelfComponent } from './users/user-info/user-shelves/shelf/shelf.component';
import { UserShelvesComponent } from './users/user-info/user-shelves/user-shelves.component';
import { UserInfoComponent } from './users/user-info/user-info.component';
import { BooksInfoComponent } from './books/books-info/books-info.component';
import { BooksListComponent } from './books/books-list/books-list.component';
import { AuthorsDiscussionsComponent } from './authors/authors-discussions/authors-discussions.component';
import { AuthorsBooksComponent } from './authors/authors-info/authors-books/authors-books.component';
import { AuthorsInfoComponent } from './authors/authors-info/authors-info.component';
import { AuthorsListComponent } from './authors/authors-list/authors-list.component';
import { AuthorsComponent } from './authors/authors.component';
import { AuthGardUserService } from './auth/auth-gard-user.service';
import { AuthGardGuestService } from './auth/auth-gard-guest.service';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { ErrorPageComponent } from './routing/error-page/error-page.component';
import { BooksComponent } from './books/books.component';
import { GenresComponent } from './genres/genres.component';


const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGardGuestService] },
  { path: 'register', component: RegisterComponent, canActivate: [AuthGardUserService] },
  { path: 'login', component: LoginComponent, canActivate: [AuthGardUserService] },
  { path: 'users/:userId', component: UserInfoComponent, children: [
    { path: 'shelves', component: UserShelvesComponent, children: [
      { path: 'noShelves', component: NoShelvesComponent},
      { path: ':shelfId', component: ShelfComponent }
    ]},
    {
      path: 'favouriteGenres', component: FavouriteGenresComponent
    },
    {
      path: 'recommendedBooks', component: RecommendedBooksComponent
    }
  ],
   canActivate: [AuthGardGuestService]},
  { path: 'authors', component: AuthorsComponent, children: [
    { path: '', component: AuthorsListComponent },
    { path: 'discussions', component: AuthorsDiscussionsComponent }],
    canActivate: [AuthGardGuestService]},
  { path: 'authors/:authorId', component: AuthorsInfoComponent, children : [
    { path: 'books', component: AuthorsBooksComponent },
    { path: 'discussions', component: AuthorDiscussionsComponent}],
    canActivate: [AuthGardGuestService]},
  { path: 'error', component: ErrorPageComponent },
  { path: 'books', component: BooksComponent, children: [
    { path: '', component: BooksListComponent },
    { path: 'discussions', component: BooksDiscussionsComponent }],
    canActivate: [AuthGardGuestService] },
  { path: 'books/:bookId', component: BooksInfoComponent, children: [
    { path: 'discussions', component: BookDiscussionsComponent}
  ], canActivate: [AuthGardGuestService] },
  { path: 'genres', component: GenresComponent, children: [
    { path: '', redirectTo: 'all', pathMatch: 'full' },
    { path: 'discussions', component: GenresDiscussionsComponent },
    { path: 'all', component: ListAllGenresComponent },
    { path: ':genreId', component: GenreInfoComponent }
  ], canActivate: [AuthGardGuestService] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGardGuestService, AuthGardUserService]
})
export class AppRoutingModule { }
