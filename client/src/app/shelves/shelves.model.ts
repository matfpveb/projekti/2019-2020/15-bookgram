import { Book } from '../books/books.model';
import { User } from '../users/user.model';

export interface Shelf {
  _id: string;
  name: string;
  user: User;
  books: Book[];
}
