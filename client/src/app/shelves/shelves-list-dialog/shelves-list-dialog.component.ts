import { NewShelfDialogComponent } from './../new-shelf-dialog/new-shelf-dialog.component';
import { ShelvesService } from './../shelves.service';
import { UserService } from './../../users/user/user.service';
import { Subscription } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { Shelf } from '../shelves.model';

@Component({
  selector: 'app-shelves-list-dialog',
  templateUrl: './shelves-list-dialog.component.html',
  styleUrls: ['./shelves-list-dialog.component.css']
})
export class ShelvesListDialogComponent implements OnInit, OnDestroy {
  public shelves: Shelf[];
  private activeSubs: Subscription[];
  private shelfChanged: boolean;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private userService: UserService,
              private shelvesService: ShelvesService,
              private dialog: MatDialog,
              private dialogRef: MatDialogRef<ShelvesListDialogComponent>) {
    this.activeSubs = [];
    this.shelfChanged = false;

    this.getShelvesByBook();
   }

  ngOnInit(): void {
  }

  getShelvesByBook() {
    const sub = this.userService.getShelvesByBook(this.data.bookId)
      .subscribe((res: Shelf[]) => {
        this.shelves = res;
      });

    this.activeSubs.push(sub);
  }

  addOrRemoveShelf(shelfId: string, isChecked: boolean) {
    let sub: Subscription;
    const body = {
      bookId : this.data.bookId
    };

    if (isChecked) {
      sub = this.shelvesService
        .addBookToShelf(shelfId, body).subscribe((res) =>
          console.log(res)
        );
    } else {
      const bookId = this.data.bookId;

      sub = this.shelvesService
        .removeBookFromShelf(shelfId, bookId).subscribe((res) =>
          this.shelfChanged = true
      );
    }

    this.activeSubs.push(sub);
  }

  onNewShelf() {
    const dialogRef = this.dialog.open(NewShelfDialogComponent,
      {
        height: '500px'
      });

    const sub = dialogRef.afterClosed()
      .subscribe(res => {
        if (res) {
          if (res.newShelf) {
            this.getShelvesByBook();
          }
        }
    });

    this.activeSubs.push(sub);
  }

  ngOnDestroy(): void {
    if (this.shelfChanged) {
      this.dialogRef.close({ shelfChanged: true});
    }

    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
