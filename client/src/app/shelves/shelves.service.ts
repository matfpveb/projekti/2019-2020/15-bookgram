import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpErrorHandler } from '../utils/http-error-handler.model';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShelvesService extends HttpErrorHandler {
  private readonly shelvesUrl = 'http://localhost:3000/shelves/';
  // tslint:disable variable-name
  private _newShelfListener = new Subject<boolean>();
  private _shelfChangedListener = new Subject<boolean>();

  constructor(router: Router,
              private http: HttpClient) {
    super(router);
  }

  public get newShelfListener() {
    return this._newShelfListener.asObservable();
  }

  public get shelfChanedListener() {
    return this._shelfChangedListener.asObservable();
  }

  onNewShelfAdded() {
    this._newShelfListener.next(true);
  }

  onShelfChanged() {
    this._shelfChangedListener.next(true);
  }

  addNewShelf(body) {
    return this.http.post(this.shelvesUrl, body)
      .pipe(catchError(super.handleError()));
  }

  addBookToShelf(shelfId: string, body) {
    return this.http.post(this.shelvesUrl + shelfId, body)
      .pipe(catchError(super.handleError()));
  }

  removeBookFromShelf(shelfId: string, bookId: string) {
    return this.http.delete(this.shelvesUrl + `${shelfId}/${bookId}`)
      .pipe(catchError(super.handleError()));
  }

  getShelfById(shelfId: string) {
    return this.http.get(this.shelvesUrl + `${shelfId}`)
    .pipe(catchError(super.handleError()));
  }

  removeShelf(shelfId: string) {
    return this.http.delete(this.shelvesUrl + shelfId)
      .pipe(catchError(super.handleError()));
  }
}
