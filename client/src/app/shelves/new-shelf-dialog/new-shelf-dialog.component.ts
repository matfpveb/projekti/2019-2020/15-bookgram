import { ShelvesService } from './../shelves.service';
import { CommonValidators } from 'ng-validator';
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: './new-shelf-dialog.component.html',
  styleUrls: ['./new-shelf-dialog.component.css']
})
export class NewShelfDialogComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public isLoading: boolean;
  public error: string;
  private activeSubs: Subscription[];

  constructor(private dialogRef: MatDialogRef<NewShelfDialogComponent>,
              private formBuilder: FormBuilder,
              private shelvesService: ShelvesService) {
    this.error = '';
    this.activeSubs = [];
    this.isLoading = false;

    this.form = this.formBuilder.group({
      name: ['', CommonValidators.requiredTrim]
    });

    const sub = this.name.valueChanges.subscribe(() => {
      if (this.error) {
        this.error = '';
      }
    });

    this.activeSubs.push(sub);
   }

  ngOnInit(): void {}

  onSubmit(data) {
    const body = {
      name: this.name.value
    };

    this.isLoading = true;

    const sub = this.shelvesService.addNewShelf(body)
      .subscribe((res) => {
        this.dialogRef.close({ newShelf: true});

        this.isLoading = false;
      }, (error) => {
        this.isLoading = false;

        this.error = 'You already have a shelf with this name';
      });

    this.activeSubs.push(sub);
  }

  public get name() {
    return this.form.get('name');
  }

  ngOnDestroy() {
    this.activeSubs.forEach((sub) => sub.unsubscribe());
  }
}
