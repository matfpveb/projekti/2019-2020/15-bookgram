import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.css'],
})
export class ErrorPageComponent implements OnInit, OnDestroy {
  public message: string;
  public statusCode: string;
  private activeSubs: Subscription[];

  constructor(private route: ActivatedRoute) {
    this.activeSubs = [];

    const sub = this.route.paramMap.subscribe((params) => {
      this.message = params.get('message');
      this.statusCode = params.get('statusCode');
    });

    this.activeSubs.push(sub);
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}

