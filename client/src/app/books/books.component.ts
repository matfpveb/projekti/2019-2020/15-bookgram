import { DiscussionsService } from 'src/app/discussions/discussions.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NewDiscussionDialogComponent } from '../discussions/new-discussion-dialog/new-discussion-dialog.component';
import { Subject, Subscribable, Subscription } from 'rxjs';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit, OnDestroy {
  private activeSubs: Subscription[];

  constructor(private dialog: MatDialog,
              private discussionsService: DiscussionsService) {
    this.activeSubs = [];
  }

  ngOnInit(): void {
  }

  onOpenDialog() {
    const dialogRef = this.dialog.open(NewDiscussionDialogComponent,
      {
        width: '520px',
      data: {
        type: 'Book'
      }
    });

    const sub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.newDiscussion) {
          this.discussionsService.onNewDiscussionAdded();
        }
      }
    });

    this.activeSubs.push(sub);
  }

  ngOnDestroy(): void {
    this.dialog.closeAll();

    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
