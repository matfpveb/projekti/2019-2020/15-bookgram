import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorHandler } from '../utils/http-error-handler.model';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService extends HttpErrorHandler {
  private readonly booksUrl = 'http://localhost:3000/books/';

  constructor(private http: HttpClient,
              router: Router) {
    super(router);
  }

  getBooks() {
    return this.http.get(this.booksUrl + `names`)
      .pipe(catchError(this.handleError()));
  }

  getAllBooks(options) {
    return this.http.get(this.booksUrl, { params: options })
      .pipe(catchError(super.handleError()));
  }

  searchBooks(options) {
    return this.http.get(this.booksUrl + 'search', { params: options })
    .pipe(catchError(super.handleError()));
  }

  getAllBooksDiscussions(options) {
    return this.http.get(this.booksUrl + `discussions`, { params: options })
      .pipe(catchError(super.handleError()));
  }

  searchBooksDiscussions(options) {
    return this.http.get(this.booksUrl + 'discussions/search', { params: options })
    .pipe(catchError(super.handleError()));
  }

  getBookById(bookId: string) {
    return this.http.get(this.booksUrl + bookId)
    .pipe(catchError(super.handleError()));
  }

  getAverageRating(bookId: string) {
    return this.http.get(this.booksUrl + `${bookId}/ratings/average`)
    .pipe(catchError(super.handleError()));
  }

  addRating(bookId: string, body: { rating: number }) {
    return this.http.post(this.booksUrl + `${bookId}/ratings`, body)
    .pipe(catchError(super.handleError()));
  }

  getReviews(bookId: string) {
    return this.http.get(this.booksUrl + `${bookId}/reviews`)
    .pipe(catchError(super.handleError()));
  }

  addReview(bookId: string, body: { review: string }) {
    return this.http.post(this.booksUrl + `${bookId}/reviews`, body)
    .pipe(catchError(super.handleError()));
  }

  removeReview(bookId: string, reviewId: string) {
    return this.http.delete(this.booksUrl + `${bookId}/reviews/${reviewId}`)
      .pipe(catchError(super.handleError()));
  }

  getAllDiscussionsByBook(bookId: string, options) {
    return this.http.get(this.booksUrl + `${bookId}/discussions`, { params: options })
      .pipe(catchError(super.handleError()));
  }

  searchDiscussionsByBook(bookId: string, options) {
    return this.http.get(this.booksUrl + `${bookId}/discussions/search`, { params: options })
    .pipe(catchError(super.handleError()));
  }
}
