import { ShelvesService } from 'src/app/shelves/shelves.service';
import { Subscription } from 'rxjs';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ShelvesListDialogComponent } from 'src/app/shelves/shelves-list-dialog/shelves-list-dialog.component';
import { Book } from '../books.model';

@Component({
  selector: 'app-books-list-image-item',
  templateUrl: './books-list-image-item.component.html',
  styleUrls: ['./books-list-image-item.component.css']
})
export class BooksListImageItemComponent implements OnInit, OnDestroy {
  @Input() book: Book;
  private activeSubs: Subscription[];

  constructor(private dialog: MatDialog,
              private shelvesService: ShelvesService) {
    this.activeSubs = [];
  }

  ngOnInit(): void {
  }

  onOpenDialog() {
    const data = {
      bookId: this.book._id
    };

    const dialogRef = this.dialog.open(ShelvesListDialogComponent,
      {
        data
      });

    const sub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.shelfChanged) {
          this.shelvesService.onShelfChanged();
        }
      }
    });

    this.activeSubs.push(sub);
  }

  ngOnDestroy(): void {
    this.dialog.closeAll();

    this.activeSubs.forEach((sub) => sub.unsubscribe());
  }
}
