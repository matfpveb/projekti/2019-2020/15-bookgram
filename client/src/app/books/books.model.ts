import { Genre } from '../genres/genres.model';
import { Author } from './../authors/authors.model';

export interface Book {
  _id: string;
  title: string;
  author: Author;
  genre: Genre;
  description: string;
  imagePath: string;
}
