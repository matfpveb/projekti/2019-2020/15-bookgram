import { Subscription } from 'rxjs';
import { DiscussionsService } from 'src/app/discussions/discussions.service';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NewDiscussionDialogComponent } from 'src/app/discussions/new-discussion-dialog/new-discussion-dialog.component';

@Component({
  selector: 'app-book-discussions',
  templateUrl: './book-discussions.component.html',
  styleUrls: ['./book-discussions.component.css']
})
export class BookDiscussionsComponent implements OnInit, OnDestroy {
  public bookId: string;
  private activeSubs: Subscription[];

  constructor(private route: ActivatedRoute,
              private dialog: MatDialog,
              private discussionsService: DiscussionsService) {
    this.activeSubs = [];

    this.getBookId();
   }

  ngOnInit(): void {
  }

  getBookId() {
    const sub = this.route.parent.paramMap
      .subscribe((params) =>
        {
          this.bookId = params.get('bookId');
        });

    this.activeSubs.push(sub);
  }

  onOpenDialog() {
    const dialogRef = this.dialog.open(NewDiscussionDialogComponent,
      {
        width: '520px',
      data: {
        type: 'Book',
        onId: this.bookId
      }
    });

    const sub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.newDiscussion) {
          this.discussionsService.onNewDiscussionAdded();
        }
      }
    });

    this.activeSubs.push(sub);
  }

  ngOnDestroy(): void {
    this.dialog.closeAll();

    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
