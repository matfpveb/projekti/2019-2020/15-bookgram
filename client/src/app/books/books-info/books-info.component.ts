import { BookReviewsListDialogComponent } from './book-reviews-list-dialog/book-reviews-list-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { BooksService } from './../books.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { switchMap, map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { ShelvesListDialogComponent } from 'src/app/shelves/shelves-list-dialog/shelves-list-dialog.component';
import { AuthorsService } from 'src/app/authors/authors.service';
import { Book } from '../books.model';

@Component({
  selector: 'app-books-info',
  templateUrl: './books-info.component.html',
  styleUrls: ['./books-info.component.css']
})
export class BooksInfoComponent implements OnInit, OnDestroy {
  public isLoading: boolean;
  public book: Book;
  public currentRate = 3;
  private activeSubs: Subscription[];

  constructor(private route: ActivatedRoute,
              private booksService: BooksService,
              private authorsService: AuthorsService,
              private dialog: MatDialog) {
    this.isLoading = true;
    this.activeSubs = [];

    this.getBookById();
  }

  ngOnInit(): void {
  }

  getBookById() {
    const sub = this.route.paramMap
      .pipe(
        map((params) => params.get('bookId')),
        switchMap(bookId =>
          this.booksService.getBookById(bookId)
        )
      )
      .subscribe((res: Book) => {
        this.book = res;
        this.isLoading = false;
      });

    this.activeSubs.push(sub);
  }

  onAddToShelf() {
    const data = {
      bookId: this.book._id
    };

    const dialogRef = this.dialog.open(ShelvesListDialogComponent,
      {
        data
      });

    const sub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log(result);
      }
    });

    this.activeSubs.push(sub);
  }

  onSeeReviews() {
    const data = {
      bookId: this.book._id
    };

    const dialogRef = this.dialog.open(BookReviewsListDialogComponent,
      {
        data
      });

    const sub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log(result);
      }
    });

    this.activeSubs.push(sub);
  }

  ngOnDestroy(): void {
    this.dialog.closeAll();

    this.activeSubs.forEach((sub) => sub.unsubscribe());
  }
}
