import { Review } from './review.model';
import { Book } from './../../../books.model';

export interface BookReviews {
  _id: string;
  book: Book;
  reviews: Review[];
}
