import { User } from './../../../../users/user.model';
import { UserService } from './../../../../users/user/user.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Review } from './review.model';

@Component({
  selector: 'app-book-reviews-item',
  templateUrl: './book-reviews-item.component.html',
  styleUrls: ['./book-reviews-item.component.css']
})
export class BookReviewsItemComponent implements OnInit {
  @Input() user: User;
  @Input() review: Review;
  @Output() deleteComment: EventEmitter<string> = new EventEmitter<string>();
  public userId: string;

  constructor(private userService: UserService) {
    this.userId = this.userService.getUserId();
  }

  ngOnInit(): void {
  }

  onRemoveComment() {
    this.deleteComment.emit(this.review._id);
  }
}
