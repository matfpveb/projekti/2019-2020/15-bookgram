import { User } from '../../../../users/user.model';

export interface Review {
  _id: string;
  user: User;
  postedAt: Date;
  review: string;
  body: string;
}
