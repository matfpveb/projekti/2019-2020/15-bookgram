import { User } from './../../../users/user.model';
import { UsersService } from './../../../users/users.service';
import { UserService } from 'src/app/users/user/user.service';
import { BooksService } from './../../books.service';
import { Component, OnInit, Inject, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Review } from './book-reviews-item/review.model';

@Component({
  selector: 'app-book-reviews-list-dialog',
  templateUrl: './book-reviews-list-dialog.component.html',
  styleUrls: ['./book-reviews-list-dialog.component.css']
})
export class BookReviewsListDialogComponent implements OnInit, OnDestroy {
  @ViewChild('exists') existingReview: ElementRef;
  public reviews: Review[];
  public user: User;
  public userId: string;
  public reviewExists: boolean;
  public form: FormGroup;
  private activeSubs: Subscription[];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private booksService: BooksService,
              private userService: UserService,
              private usersService: UsersService,
              private formBuilder: FormBuilder) {
    this.activeSubs = [];
    this.reviews = [];
    this.reviewExists = false;
    this.form = this.formBuilder.group({
      review: ['My review']
    });

    this.getCurrentUser();
    this.getReviews();
  }

  ngOnInit(): void {
  }

  getCurrentUser() {
    this.userId = this.userService.getUserId();

    const sub = this.usersService.getUserById(this.userId)
      .subscribe((res: User) => {
        this.user = res;
      });

    this.activeSubs.push(sub);
  }

  getReviews() {
    const bookId = this.data.bookId;

    const sub = this.booksService.getReviews(bookId)
      .subscribe((res: { reviews: Review[]}) => {
        if (res.reviews.length > 0) {
          const currentReview = res.reviews.find((review) => review.user._id === this.userId);
          this.reviewExists = !!currentReview;
          if (this.reviewExists) {
            this.form.setValue({review: currentReview.review});
          }
        }
        this.reviews = res.reviews;
      });

    this.activeSubs.push(sub);
  }

  removeReview(reviewId: string) {
    const bookId = this.data.bookId;

    const sub = this.booksService.removeReview(bookId, reviewId)
      .subscribe(res => {
        this.getReviews();
        this.review.setValue('My review');
      });

    this.activeSubs.push(sub);
  }

  scroll() {
      this.existingReview.nativeElement.scrollIntoView();
      this.existingReview.nativeElement.focus({preventScroll: true});
  }

  onSubmit() {
    const body = {
      review: this.review.value
    };

    const sub = this.booksService.addReview(this.data.bookId, body)
      .subscribe((res) => {
        this.getReviews();
      });

    this.activeSubs.push(sub);
  }

  public get review() {
    return this.form.get('review');
  }

  ngOnDestroy(): void {
    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
