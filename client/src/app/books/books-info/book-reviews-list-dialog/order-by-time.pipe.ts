import { Pipe, PipeTransform } from '@angular/core';
import { Review } from './book-reviews-item/review.model';

@Pipe({
  name: 'orderByTime'
})
export class OrderByTimePipe implements PipeTransform {

  transform(reviews: Review[]): any {
    if (reviews == null) {
        return [];
    }
    return reviews.sort((a, b) => {
      return new Date(a.postedAt).getTime() - new Date(b.postedAt).getTime();
    });
  }
}
