import { BookRatings } from './book-ratings.model';
import { BooksService } from './../../books.service';
import { Subscription } from 'rxjs';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { UserService } from 'src/app/users/user/user.service';

@Component({
  selector: 'app-book-rating',
  templateUrl: './book-rating.component.html',
  styleUrls: ['./book-rating.component.css']
})
export class BookRatingComponent implements OnInit, OnDestroy {
  @Input() bookId: string;
  public currentRating: number;
  public avgRating: number;
  private activeSubs: Subscription[];

  constructor(private userService: UserService,
              private booksService: BooksService) {
    this.currentRating = 0;
    this.avgRating = 0;
    this.activeSubs = [];
  }

  ngOnInit(): void {
    this.getUserRating();

    this.getAverageRating();
  }

  getUserRating() {
    const sub = this.userService.getRatingByBook(this.bookId)
      .subscribe((res: BookRatings) => {
        if (res) {
          this.currentRating = res.ratings[0].rating;
        }
      });

    this.activeSubs.push(sub);
  }

  getAverageRating() {
    const sub = this.booksService.getAverageRating(this.bookId)
      .subscribe((res: { avgRating: number}[]) => {
        if (res.length > 0) {
          this.avgRating = res[0].avgRating || 0;
        }
      });

    this.activeSubs.push(sub);
  }

  onRateChange(rating: number) {
    const body = {
      rating
    };

    const sub = this.booksService.addRating(this.bookId, body)
      .subscribe((res) => {
        this.getAverageRating();
      });

    this.activeSubs.push(sub);
  }

  ngOnDestroy(): void {
    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
