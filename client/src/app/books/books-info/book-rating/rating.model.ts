import { User } from 'src/app/users/user.model';

export interface Rating {
  _id: string;
  user: User;
  rating: number;
  postedAt: Date;
}
