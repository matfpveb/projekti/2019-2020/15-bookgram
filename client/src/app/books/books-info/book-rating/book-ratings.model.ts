import { Book } from '../../books.model';
import { Rating } from './rating.model';

export interface BookRatings {
  book: Book;
  ratings: Rating[];
}
