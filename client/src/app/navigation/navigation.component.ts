import { User } from './../users/user.model';
import { Router } from '@angular/router';
import { AuthService } from './../auth/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit, OnDestroy {
  private authListenerSub: Subscription;
  public isAuth: boolean;

  constructor(private authService: AuthService,
              private router: Router) {
    const token = localStorage.getItem('token');

    if (token) {
      this.isAuth = true;
    } else {
      this.isAuth = false;
    }

    this.authListenerSub = this.authService.authStatusListener
      .subscribe((isAuth) => {
        this.isAuth = isAuth;
    });
  }

  ngOnInit(): void {
  }

  onLogout() {
    this.authService.logout();

    this.router.navigate(['/login']);
  }
  ngOnDestroy(): void {
    this.authListenerSub.unsubscribe();
  }
}
