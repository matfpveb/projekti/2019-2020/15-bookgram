import { Discussion } from './../discussion.model';
import { DiscussionsService } from 'src/app/discussions/discussions.service';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { DiscussionsInfoDialogComponent } from '../discussions-info-dialog/discussions-info-dialog.component';

@Component({
  selector: 'app-discussions-list-smaller-item',
  templateUrl: './discussions-list-smaller-item.component.html',
  styleUrls: ['./discussions-list-smaller-item.component.css']
})
export class DiscussionsListSmallerItemComponent implements OnInit, OnDestroy {
  @Input() discussion: Discussion;
  private activeSubs: Subscription[];

  constructor(private dialog: MatDialog,
              private discussionsService: DiscussionsService) {
    this.activeSubs = [];
   }

  ngOnInit(): void {
  }

  onOpenDiscussion() {
    const data = {
      discussionId: this.discussion._id
    };

    const dialogRef = this.dialog.open(DiscussionsInfoDialogComponent,
      {
        width: '570px',
        data
      });

    const sub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.discussionRemoved) {
          this.discussionsService.onNewDiscussionAdded();
        }
      }
    });

    this.activeSubs.push(sub);
  }

  ngOnDestroy(): void {
    this.dialog.closeAll();

    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
