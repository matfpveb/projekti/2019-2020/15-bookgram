import { GenresService } from '../../genres/genres.service';
import { DiscussionsService } from '../discussions.service';
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { AuthorsService } from 'src/app/authors/authors.service';
import { BooksService } from 'src/app/books/books.service';

@Component({
  selector: 'app-new-discussion-dialog',
  templateUrl: './new-discussion-dialog.component.html',
  styleUrls: ['./new-discussion-dialog.component.css']
})
export class NewDiscussionDialogComponent implements OnInit, OnDestroy {
  public isLoading: boolean;
  public form: FormGroup;
  public datas;
  public filteredOptions: Observable<any[]>;
  private activeSubs: Subscription[];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private formBuilder: FormBuilder,
              private discussionsService: DiscussionsService,
              private authorsService: AuthorsService,
              private booksService: BooksService,
              private genresService: GenresService,
              private dialogRef: MatDialogRef<NewDiscussionDialogComponent>) {
    this.activeSubs = [];
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      about: [''],
      chooseData: ['', Validators.required]
    });

    if (!this.data.onId) {
      this.getAllData();
      this.filteredOptions = this.chooseData.valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : ( value.name || value.title)),
        map(name => name ? this._filter(name) : this.datas.slice())
      );
    } else {
      this.chooseData.setValue('Ok');
    }
  }

  ngOnInit(): void {
  }

   private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.datas.filter(option =>  this.data.type === 'Book' ?
      option.title.toLowerCase().includes(filterValue) :
      option.name.toLowerCase().includes(filterValue) );
  }

  getAllData() {
    if (this.data.type === 'Author') {
      this.getAllAuthors();
    } else if (this.data.type === 'Book') {
      this.getAllBooks();
    } else if (this.data.type === 'Genre') {
      this.getAllGenres();
    }
  }

  getAllAuthors() {
    this.isLoading = true;

    const sub = this.authorsService.getAuthors().subscribe(
      (authors => {
        this.datas = authors;
        this.isLoading = false;
      })
    );

    this.activeSubs.push(sub);
  }

  getAllBooks() {
    this.isLoading = true;

    const sub = this.booksService.getBooks().subscribe(
      (books => {
        this.datas = books;
        this.isLoading = false;
      })
    );

    this.activeSubs.push(sub);
  }

  getAllGenres() {
    this.isLoading = true;

    const sub = this.genresService.getGenresList().subscribe(
      (genres => {
        this.datas = genres;
        this.isLoading = false;
      })
    );

    this.activeSubs.push(sub);
  }

  onSubmit(data) {
    const body: any = {
      name: data.name,
      onModel: this.data.type,
      onId: this.data.onId || data.chooseData._id
    };

    const about = data.about.trim();
    if (about) {
      body.description = about;
    }

    this.isLoading = true;

    const sub = this.discussionsService.addNewDiscussion(body).subscribe((res) => {
      this.dialogRef.close({ newDiscussion: true});

      this.isLoading = false;
    });

    this.activeSubs.push(sub);
  }

  public get chooseData() {
    return this.form.get('chooseData');
  }

  displayFn(datas): string {
    return datas ? (datas.name || datas.title) : '';
  }

  ngOnDestroy() {
    this.activeSubs.forEach(sub => sub.unsubscribe());
  }
}
