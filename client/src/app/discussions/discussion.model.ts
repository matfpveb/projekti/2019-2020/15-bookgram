import { Review } from '../books/books-info/book-reviews-list-dialog/book-reviews-item/review.model';
import { User } from '../users/user.model';
import { Author } from '../authors/authors.model';
import { Book } from '../books/books.model';
import { Genre } from '../genres/genres.model';

export interface Discussion {
  _id: string;
  name: string;
  description: string;
  onModel: string;
  onId;
  user: User;
  createdAt: Date;
  comments: Review[];
}
