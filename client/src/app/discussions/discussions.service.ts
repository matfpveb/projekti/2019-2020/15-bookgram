import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorHandler } from '../utils/http-error-handler.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DiscussionsService extends HttpErrorHandler {
  private readonly discussionsUrl = 'http://localhost:3000/discussions/';
  // tslint:disable: variable-name
  private _newDiscussionListener = new Subject<boolean>();

  constructor(private http: HttpClient,
              router: Router) {
      super(router);
  }

  public get newDiscussionListener() {
    return this._newDiscussionListener.asObservable();
  }

  onNewDiscussionAdded() {
        this._newDiscussionListener.next(true);
  }

  addNewDiscussion(body) {
    return this.http.post(this.discussionsUrl, body);
  }

  getAllAuthorDiscussions(options) {
    return this.http.get(this.discussionsUrl + 'authors', { params: options }
    );
  }

  searchAuthorDiscussions(options) {
    return this.http.get(this.discussionsUrl + 'authors/search', { params: options });
  }

  getDiscussionById(discussionId: string) {
    return this.http.get(this.discussionsUrl + discussionId)
      .pipe(catchError(super.handleError()));
  }

  addNewComment(discussionId: string, body: { comment: string }) {
    return this.http.post(this.discussionsUrl + discussionId, body)
      .pipe(catchError(super.handleError()));
  }

  removeComment(discussionId: string, commentId: string) {
    return this.http.delete(this.discussionsUrl + `${discussionId}/${commentId}`)
      .pipe(catchError(super.handleError()));
  }

  removeDiscussion(discussionId: string) {
    return this.http.delete(this.discussionsUrl + discussionId)
      .pipe(catchError(super.handleError()));
  }
}
