import { Discussion } from './../discussion.model';
import { Review } from '../../books/books-info/book-reviews-list-dialog/book-reviews-item/review.model';
import { DiscussionsService } from './../discussions.service';
import { Subscription } from 'rxjs';
import { DiscussionsInfoDialogComponent } from './../discussions-info-dialog/discussions-info-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-discussions-list-item',
  templateUrl: './discussions-list-item.component.html',
  styleUrls: ['./discussions-list-item.component.css']
})
export class DiscussionsListItemComponent implements OnInit, OnDestroy {
  @Input() discussion: Discussion;
  private activeSubs: Subscription[];

  constructor(private dialog: MatDialog,
              private discussionsService: DiscussionsService) {
    this.activeSubs = [];
   }

  ngOnInit(): void {
  }

  onOpenDiscussion() {
    const data = {
      discussionId: this.discussion._id
    };

    const dialogRef = this.dialog.open(DiscussionsInfoDialogComponent,
      { width: '570px',
        data
      });

    const sub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.discussionRemoved) {
          this.discussionsService.onNewDiscussionAdded();
        }
      }
    });

    this.activeSubs.push(sub);
  }

  ngOnDestroy(): void {
    this.dialog.closeAll();

    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
