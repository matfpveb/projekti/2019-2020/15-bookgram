import { Discussion } from './../discussion.model';
import { UserService } from './../../users/user/user.service';
import { CommonValidators } from 'ng-validator';
import { Subscription } from 'rxjs';
import { DiscussionsService } from './../discussions.service';
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-discussions-info-dialog',
  templateUrl: './discussions-info-dialog.component.html',
  styleUrls: ['./discussions-info-dialog.component.css']
})
export class DiscussionsInfoDialogComponent implements OnInit, OnDestroy {
  public isLoading: boolean;
  public form: FormGroup;
  public discussion: Discussion;
  public userId: string;
  private activeSubs: Subscription[];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private discussionsService: DiscussionsService,
              private formBuilder: FormBuilder,
              private userService: UserService,
              private dialogRef: MatDialogRef<DiscussionsInfoDialogComponent>) {
    this.isLoading = true;
    this.activeSubs = [];

    this.form = this.formBuilder.group({
      comment: ['', CommonValidators.requiredTrim]
    });

    this.userId = this.userService.getUserId();

    this.getDiscussionById();
  }

  ngOnInit(): void {
  }

  getDiscussionById() {
    const sub = this.discussionsService.getDiscussionById(this.data.discussionId).
      subscribe((res: Discussion) => {
        this.discussion = res;

        this.isLoading = false;
      });

    this.activeSubs.push(sub);
  }

  onSubmit(data) {
    const body = {
      comment: this.comment.value.trim()
    };

    const sub = this.discussionsService.addNewComment(this.discussion._id, body)
      .subscribe(res => {
        this.getDiscussionById();
      });

    this.activeSubs.push(sub);
  }

  onDeleteComment(commentId: string) {
    const discussionId = this.discussion._id;

    const sub = this.discussionsService.removeComment(discussionId, commentId)
    .subscribe(res => {
      this.getDiscussionById();
    });

    this.activeSubs.push(sub);
  }

  public get comment() {
    return this.form.get('comment');
  }

  onRemoveDiscussion() {
    const sub = this.discussionsService
      .removeDiscussion(this.discussion._id)
      .subscribe();

    this.activeSubs.push(sub);
    this.dialogRef.close({ discussionRemoved: true});
  }

  ngOnDestroy(): void {
    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
