import { UserService } from 'src/app/users/user/user.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonValidators } from 'ng-validator';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  public error: string;
  public isLoading: boolean;
  public form: FormGroup;
  private activeSubs: Subscription[];

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private userService: UserService) {
    this.error = '';
    this.activeSubs = [];

    this.form = this.formBuilder.group({
      username: ['', [CommonValidators.requiredTrim]],
      password: ['', [Validators.required]]
    });

    const sub = this.form.valueChanges.subscribe((data) => {
      if (this.error) {
        this.error = '';
      }
    });

    this.activeSubs.push(sub);
  }

  ngOnInit(): void { }

  onSubmit() {
    this.isLoading = true;

    const data = {
      username: this.username.value,
      password: this.password.value
    };

    const sub = this.authService.login(data)
      .subscribe( res => {
        this.isLoading = false;

        const userId = this.userService.getUserId();
        this.router.navigate(['/users', userId]);
      }, error => {
        this.isLoading = false;

        this.error = error;
      });

    this.activeSubs.push(sub);
  }

  public get username() {
    return this.form.get('username');
  }
  public get password() {
    return this.form.get('password');
  }
  public get password2() {
    return this.form.get('password2');
  }

  ngOnDestroy(): void {
    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
