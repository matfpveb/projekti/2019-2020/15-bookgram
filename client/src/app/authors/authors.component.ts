import { Subscription } from 'rxjs';
import { DiscussionsService } from 'src/app/discussions/discussions.service';
import { NewDiscussionDialogComponent } from '../discussions/new-discussion-dialog/new-discussion-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit, OnDestroy {
  private activeSubs: Subscription[];

  constructor(private dialog: MatDialog,
              private discussionsService: DiscussionsService) {
    this.activeSubs = [];
  }

  ngOnInit(): void { }

  onOpenDialog() {
    const dialogRef = this.dialog.open(NewDiscussionDialogComponent,
      {
        width: '520px',
      data: {
        type: 'Author'
      }
    });

    const sub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.newDiscussion) {
          this.discussionsService.onNewDiscussionAdded();
        }
      }
    });

    this.activeSubs.push(sub);
  }

  ngOnDestroy(): void {
    this.dialog.closeAll();

    this.activeSubs.forEach(sub => sub.unsubscribe());
  }
}
