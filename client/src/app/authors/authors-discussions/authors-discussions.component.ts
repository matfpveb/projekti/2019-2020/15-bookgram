import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-authors-discussions',
  templateUrl: './authors-discussions.component.html',
  styleUrls: ['./authors-discussions.component.css']
})
export class AuthorsDiscussionsComponent implements OnInit {
  constructor() { }

  ngOnInit(): void { }
}
