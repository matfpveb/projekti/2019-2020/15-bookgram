import { Author } from './../../authors.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-authors-item',
  templateUrl: './authors-item.component.html',
  styleUrls: ['./authors-item.component.css']
})
export class AuthorsItemComponent implements OnInit {
  @Input() author: Author;

  constructor() { }

  ngOnInit(): void { }

}
