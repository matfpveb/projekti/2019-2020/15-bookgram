import { Observable } from 'rxjs';
import { AuthorsService } from './../authors.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonValidators } from 'ng-validator';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-authors-list',
  templateUrl: './authors-list.component.html',
  styleUrls: ['./authors-list.component.css']
})
export class AuthorsListComponent implements OnInit  {
  constructor() { }

  ngOnInit() { }
}
