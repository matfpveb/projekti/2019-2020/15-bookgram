export interface Author {
  _id: string;
  name: string;
  bio: string;
  imagePath: string;
}
