import { Subscription } from 'rxjs';
import { DiscussionsService } from 'src/app/discussions/discussions.service';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NewDiscussionDialogComponent } from 'src/app/discussions/new-discussion-dialog/new-discussion-dialog.component';

@Component({
  selector: 'app-author-discussions',
  templateUrl: './author-discussions.component.html',
  styleUrls: ['./author-discussions.component.css']
})
export class AuthorDiscussionsComponent implements OnInit, OnDestroy {
  public authorId: string;
  private activeSubs: Subscription[];

  constructor(private route: ActivatedRoute,
              private dialog: MatDialog,
              private discussionsService: DiscussionsService) {
    this.activeSubs = [];

    this.getAuthorId();
   }

  ngOnInit(): void { }

  getAuthorId() {
    const sub = this.route.parent.paramMap
      .subscribe((params) => {
          this.authorId = params.get('authorId');
      });

    this.activeSubs.push(sub);
  }

  onOpenDialog() {
    const dialogRef = this.dialog.open(NewDiscussionDialogComponent,
      {
        width: '520px',
      data: {
        type: 'Author',
        onId: this.authorId
      }
    });

    const sub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.newDiscussion) {
          this.discussionsService.onNewDiscussionAdded();
        }
      }
    });

    this.activeSubs.push(sub);
  }

  ngOnDestroy(): void {
    this.dialog.closeAll();

    this.activeSubs.forEach((sub) => sub.unsubscribe());
  }
}
