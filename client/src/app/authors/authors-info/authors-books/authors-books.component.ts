import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthorsService } from './../../authors.service';
import { BooksService } from './../../../books/books.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { map, switchMap } from 'rxjs/operators';
import { Book } from 'src/app/books/books.model';

@Component({
  selector: 'app-authors-books',
  templateUrl: './authors-books.component.html',
  styleUrls: ['./authors-books.component.css']
})
export class AuthorsBooksComponent implements OnInit, OnDestroy {
  public books: Book[];
  private activeSubs: Subscription[];

  constructor(private route: ActivatedRoute,
              private authorsService: AuthorsService) {
    this.books = [];
    this.activeSubs = [];

    this.getBooks();
  }

  ngOnInit(): void { }

  getBooks() {
    const sub = this.route.parent.paramMap
    .pipe(
      map((params) => params.get('authorId')),
      switchMap(authorId =>
        this.authorsService.getAuthorBooks(authorId)
      )
    )
    .subscribe((res: Book[]) => {
      this.books = res;
    });

    this.activeSubs.push(sub);
  }

  ngOnDestroy(): void {
    this.activeSubs.forEach(sub => sub.unsubscribe());
  }
}
