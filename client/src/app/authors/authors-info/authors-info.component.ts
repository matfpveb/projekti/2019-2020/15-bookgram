import { Author } from './../authors.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { AuthorsService } from '../authors.service';

@Component({
  selector: 'app-authors-info',
  templateUrl: './authors-info.component.html',
  styleUrls: ['./authors-info.component.css']
})
export class AuthorsInfoComponent implements OnInit, OnDestroy {
  public author: Author;
  public isLoading: boolean;
  private activeSubs: Subscription[];

  constructor(private route: ActivatedRoute,
              private authorsService: AuthorsService,
              private router: Router) {
    this.isLoading = true;
    this.activeSubs = [];

    this.getAuthorById();
  }

  ngOnInit(): void { }

  getAuthorById() {
    const sub = this.route.paramMap
      .pipe(
        map((params) => params.get('authorId')),
        switchMap(authorId =>
          this.authorsService.getAuthorById(authorId)
        )
      )
      .subscribe((res: Author) => {
        this.author = res;
        this.isLoading = false;
      });

    this.activeSubs.push(sub);
  }

  ngOnDestroy(): void {
    this.activeSubs.forEach(sub => sub.unsubscribe());
  }
}
