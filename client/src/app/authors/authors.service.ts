import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorHandler } from '../utils/http-error-handler.model';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService extends HttpErrorHandler {
  private readonly authorsUrl = 'http://localhost:3000/authors/';

  constructor(private http: HttpClient, router: Router) {
    super(router);
   }

  getAuthors() {
    return this.http.get(this.authorsUrl + `names`)
      .pipe(catchError(super.handleError()));
  }

  getAllAuthors(options) {
    return this.http.get(this.authorsUrl, { params: options })
      .pipe(catchError(super.handleError()));
  }

  searchAuthors(options) {
    return this.http.get(this.authorsUrl + 'search', { params: options })
    .pipe(catchError(super.handleError()));
  }

  getAuthorById(authorId: string) {
    return this.http.get(this.authorsUrl + authorId)
    .pipe(catchError(super.handleError()));
  }

  getAuthorBooks(authorId: string) {
    return this.http.get(this.authorsUrl + `${authorId}/books`)
      .pipe(catchError(super.handleError()));
  }

  getAllAuthorsDiscussions(options) {
    return this.http.get(this.authorsUrl + `discussions`, { params: options })
      .pipe(catchError(super.handleError()));
  }

  searchAuthorsDiscussions(options) {
    return this.http.get(this.authorsUrl + 'discussions/search', { params: options })
      .pipe(catchError(super.handleError()));
  }

  getDiscussionsByAuthor(authorId: string, options) {
    return this.http.get(this.authorsUrl + `${authorId}/discussions`, { params: options })
      .pipe(catchError(super.handleError()));
  }

  searchDiscussionsByAuthor(authorId: string, options) {
    return this.http.get(this.authorsUrl + `${authorId}/discussions/search`, { params: options })
      .pipe(catchError(super.handleError()));
  }
}
