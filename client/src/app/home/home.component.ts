import { Router } from '@angular/router';
import { UserService } from 'src/app/users/user/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor(private userService: UserService,
              private router: Router) {
    const userId = this.userService.getUserId();

    this.router.navigate(['/users', userId]);
  }

  ngOnInit(): void {
  }

}
