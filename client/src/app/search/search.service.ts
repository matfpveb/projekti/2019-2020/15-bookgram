import { GenresService } from './../genres/genres.service';
import { UsersService } from './../users/users.service';
import { BooksService } from './../books/books.service';
import { DiscussionsService } from './../discussions/discussions.service';
import { AuthorsService } from './../authors/authors.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private authorsService: AuthorsService,
              private booksService: BooksService,
              private discussionsService: DiscussionsService,
              private usersService: UsersService,
              private genresService: GenresService) { }

  getAllData(type: string, options, id: string) {
    switch (type) {
      case 'AUTHOR':
        return this.authorsService.getAllAuthors(options);
        break;
      case 'AUTHOR_DISCUSSIONS':
        if (id) {
          return this.authorsService.getDiscussionsByAuthor(id, options);
        } else {
         return this.authorsService.getAllAuthorsDiscussions(options);
        }
        break;
      case 'BOOK':
        return this.booksService.getAllBooks(options);
        break;
      case 'BOOK_DISCUSSIONS':
        if (id) {
          return this.booksService.getAllDiscussionsByBook(id, options);
        } else {
          return this.booksService.getAllBooksDiscussions(options);
        }
        break;
      case 'USER_DISCUSSIONS':
        return this.usersService.getAllUserDiscussions(id, options);
        break;
      case 'GENRE_DISCUSSIONS':
        return this.genresService.getAllGenresDiscussions(options);
        break;
    }
  }

  searchData(type: string, options, id: string) {
    switch (type) {
      case 'AUTHOR':
        return this.authorsService.searchAuthors(options);
        break;
      case 'AUTHOR_DISCUSSIONS':
        if (id) {
          return this.authorsService.searchDiscussionsByAuthor(id, options);
        } else {
          return this.authorsService.searchAuthorsDiscussions(options);
        }
        break;
      case 'BOOK':
        return this.booksService.searchBooks(options);
        break;
      case 'BOOK_DISCUSSIONS':
        if (id) {
          return this.booksService.searchDiscussionsByBook(id, options);
        } else {
          return this.booksService.searchBooksDiscussions(options);
        }
        break;
      case 'USER_DISCUSSIONS':
        return this.usersService.searchUserDiscussions(id, options);
        break;
      case 'GENRE_DISCUSSIONS':
        return this.genresService.searchGenresDiscussions(options);
        break;
    }
  }

}
