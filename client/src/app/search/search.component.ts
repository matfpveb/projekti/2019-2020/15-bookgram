import { DiscussionsService } from 'src/app/discussions/discussions.service';
import { SearchService } from './search.service';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonValidators } from 'ng-validator';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, OnDestroy {
  @Input() type: string;
  @Input() id: string;
  public searchForm: FormGroup;
  public length: number;
  public pageSize: number;
  public pageIndex: number;
  public isLoading = true;
  public data: any;
  private newDiscussionListener: Subscription;
  private searchParams: string;
  private activeSubs: Subscription[];

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private searchService: SearchService,
              private discussionsService: DiscussionsService) {
    this.data = [];
    this.activeSubs = [];

    this.searchForm = this.formBuilder.group({
      searchTerm: ['', CommonValidators.requiredTrim]
    });
  }

  ngOnInit() {
    this.newDiscussionListener = this.discussionsService.newDiscussionListener
      .subscribe((newDiscussion: boolean) => {
        if (newDiscussion) {
          this.startSearch();
        }
    });

    this.startSearch();
  }

  startSearch() {
    const sub = this.route.queryParams
    .subscribe((params) => {
        this.searchParams = params.search;

        const page = params.page || 1;

        const options: any = {
          page,
          limit: 5
        };

        this.pageIndex = page - 1;
        this.getData(options);
    });

    this.activeSubs.push(sub);
  }

  onSearch() {
    const term = this.searchTerm.value.trim();

    this.router.navigate([], {queryParams: {search: term}});

    this.searchForm.reset();
  }

  getData(options) {
    let obs: Observable<any>;

    if (this.searchParams) {
      options.search = this.searchParams;

      obs = this.searchService.searchData(this.type, options, this.id);
    } else {
      obs = this.searchService.getAllData(this.type, options, this.id);
    }

    const newSub = obs.subscribe((res) => this.handlePagination(res));
    this.activeSubs.push(newSub);
  }


  onPageChange(event: PageEvent) {
    if (this.searchParams) {
          this.router.navigate([], {queryParams: { search: this.searchParams, page: event.pageIndex + 1}});
    } else {
      this.router.navigate([], {queryParams: { page: event.pageIndex + 1}});
    }

  }

  public get searchTerm() {
    return this.searchForm.get('searchTerm');
  }

  private handlePagination(res) {
    this.data = res.docs;
    this.isLoading = false;

    if (res.page > res.totalPages) {
      this.router.navigate([], {queryParams: {page: res.totalPages}});
    } else {
      this.pageIndex = res.page - 1;
      this.pageSize = res.limit;
      this.length = res.totalDocs;
    }
  }

  ngOnDestroy() {
    this.newDiscussionListener.unsubscribe();

    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}

