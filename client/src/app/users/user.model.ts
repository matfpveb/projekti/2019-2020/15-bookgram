export interface User {
  _id: string;
  username: string;
  firstName?: string;
  lastName?: string;
  birthday?: string;
  about?: string;
  imagePath?: string;
}
