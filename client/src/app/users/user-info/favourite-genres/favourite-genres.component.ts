import { ActivatedRoute } from '@angular/router';
import { UsersService } from './../../users.service';
import { Component, OnInit } from '@angular/core';
import { map, switchMap } from 'rxjs/operators';
import { Genre } from 'src/app/genres/genres.model';

@Component({
  selector: 'app-favourite-genres',
  templateUrl: './favourite-genres.component.html',
  styleUrls: ['./favourite-genres.component.css']
})
export class FavouriteGenresComponent implements OnInit {
  public favouriteGenres: Genre[];

  constructor(private route: ActivatedRoute,
              private usersService: UsersService) {

    this.getAllFavouriteGenres();
  }

  getAllFavouriteGenres() {
    const sub = this.route.parent.paramMap
    .pipe(
      map((params) => params.get('userId')),
      switchMap(userId =>
        this.usersService.getFavouriteGenres(userId)
      )
    )
    .subscribe((res: { favouriteGenres: Genre[] }) => {
      this.favouriteGenres = res.favouriteGenres;
    });
  }

  ngOnInit(): void {
  }

}
