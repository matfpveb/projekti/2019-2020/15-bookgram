import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-favourite-genres',
  templateUrl: './no-favourite-genres.component.html',
  styleUrls: ['./no-favourite-genres.component.css']
})
export class NoFavouriteGenresComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
