import { User } from './../user.model';
import { ActivatedRoute } from '@angular/router';
import { UserService } from './../user/user.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { map, switchMap } from 'rxjs/operators';
import { UsersService } from '../users.service';
import { Subject, Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit, OnDestroy {
  public isLoading: boolean;
  public user: User;
  public currentUserId: string;
  private userChangeListener: Subscription;
  private activeSubs: Subscription[];

  constructor(private route: ActivatedRoute,
              private usersService: UsersService,
              private userService: UserService) {
    this.isLoading = true;
    this.activeSubs = [];

    this.getUserById();
    this.getCurrentUserId();
   }

   ngOnInit() {
    this.userChangeListener = this.userService.userChangeListener
      .subscribe((userChanged: boolean) => {
        if (userChanged) {
          this.getUserById();
        }
    });
  }

  getCurrentUserId() {
    this.currentUserId = this.userService.getUserId();
  }

  getUserById() {
    const sub = this.route.paramMap
      .pipe(
        map((params) => params.get('userId')),
        switchMap(userId =>
          this.usersService.getUserById(userId)
        )
      )
      .subscribe((res: User) => {
        this.user = res;
        this.isLoading = false;
      });

    this.activeSubs.push(sub);
  }

  ngOnDestroy(): void {
    this.userChangeListener.unsubscribe();

    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
