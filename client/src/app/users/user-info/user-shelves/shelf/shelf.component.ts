import { UserService } from 'src/app/users/user/user.service';
import { MatDialog } from '@angular/material/dialog';
import { ShelvesService } from './../../../../shelves/shelves.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { map, switchMap } from 'rxjs/operators';
import { NewShelfDialogComponent } from 'src/app/shelves/new-shelf-dialog/new-shelf-dialog.component';
import { Book } from 'src/app/books/books.model';

@Component({
  selector: 'app-shelf',
  templateUrl: './shelf.component.html',
  styleUrls: ['./shelf.component.css']
})
export class ShelfComponent implements OnInit, OnDestroy {
  public books: Book[];
  public loggedUserId: string;
  public userId: string;
  private shelfChangedListener: Subscription;
  private activeSubs: Subscription[];

  constructor(private route: ActivatedRoute,
              private shelvesService: ShelvesService,
              private dialog: MatDialog,
              private router: Router,
              private userService: UserService) {
    this.books = [];
    this.activeSubs = [];

    this.loggedUserId = this.userService.getUserId();
    this.getUserId();

    this.getShelfById();

    this.shelfChangedListener = this.shelvesService.shelfChanedListener
      .subscribe((res: boolean) => {
        if (res) {
          this.getShelfById();
        }
      });
  }

  ngOnInit(): void {}

  getUserId() {
    const sub = this.route.parent.parent.paramMap
      .subscribe((params) => {
          this.userId = params.get('userId');
      });

    this.activeSubs.push(sub);
  }

  getShelfById() {
    const sub = this.route.paramMap
    .pipe(
      map((params) => params.get('shelfId')),
      switchMap(shelfId =>
        this.shelvesService.getShelfById(shelfId)
      )
    )
    .subscribe((res: any) => {
      this.books = res.books;
    });

    this.activeSubs.push(sub);
  }

  onNewShelf() {
    const dialogRef = this.dialog.open(NewShelfDialogComponent,
      {
        height: '500px'
      });

    const sub = dialogRef.afterClosed()
      .subscribe(res => {
        if (res) {
          if (res.newShelf) {
            this.shelvesService.onNewShelfAdded();
          }
        }
    });

    this.activeSubs.push(sub);
  }

  onDeleteShelf() {
    const sub = this.route.paramMap
    .pipe(
      map((params) => params.get('shelfId')),
      switchMap(shelfId =>
        this.shelvesService.removeShelf(shelfId)
      )
    )
    .subscribe((res: {msg: string}) => {
      this.shelvesService.onNewShelfAdded();

      this.router.navigate(['users', this.userId, 'shelves']);
    });
  }

  ngOnDestroy(): void {
    this.dialog.closeAll();

    this.shelfChangedListener.unsubscribe();
    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
