import { ShelvesService } from './../../../shelves/shelves.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscribable, Subscription } from 'rxjs';
import { UsersService } from './../../users.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { map, switchMap } from 'rxjs/operators';
import { Shelf } from 'src/app/shelves/shelves.model';

@Component({
  selector: 'app-user-shelves',
  templateUrl: './user-shelves.component.html',
  styleUrls: ['./user-shelves.component.css']
})
export class UserShelvesComponent implements OnInit, OnDestroy {
  public shelves: Shelf[];
  private newShelfListener: Subscription;
  private activeSubs: Subscription[];

  constructor(private route: ActivatedRoute,
              private usersService: UsersService,
              private shelvesService: ShelvesService,
              private router: Router) {
    this.shelves = [];
    this.activeSubs = [];

    this.newShelfListener = this.shelvesService.newShelfListener
      .subscribe((newShelf) => {
        if (newShelf) {
          this.getShelves();
        }
    });

    this.getShelves();
  }

  ngOnInit(): void {
  }

  getShelves() {
    const sub = this.route.parent.paramMap
    .pipe(
      map((params) => params.get('userId')),
      switchMap(userId =>
        this.usersService.getShelves(userId)
      )
    )
    .subscribe((res: Shelf[]) => {
      this.shelves = res;
      if (this.shelves.length > 0) {
        this.router.navigate([this.shelves[0]._id], {relativeTo: this.route} );
      } else {
        this.router.navigate(['noShelves'], { relativeTo: this.route });
      }
    });

    this.activeSubs.push(sub);
  }

  ngOnDestroy(): void {
    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
