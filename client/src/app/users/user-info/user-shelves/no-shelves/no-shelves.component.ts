import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { NewShelfDialogComponent } from 'src/app/shelves/new-shelf-dialog/new-shelf-dialog.component';
import { ShelvesService } from 'src/app/shelves/shelves.service';
import { UserService } from 'src/app/users/user/user.service';

@Component({
  selector: 'app-no-shelves',
  templateUrl: './no-shelves.component.html',
  styleUrls: ['./no-shelves.component.css']
})
export class NoShelvesComponent implements OnInit, OnDestroy {
  public loggedUserId: string;
  public userId: string;
  private activeSubs: Subscription[];

  constructor(private route: ActivatedRoute,
              private shelvesService: ShelvesService,
              private dialog: MatDialog,
              private router: Router,
              private userService: UserService) {
    this.activeSubs = [];

    this.loggedUserId = this.userService.getUserId();
    this.getUserId();
  }

  ngOnInit(): void {}

  getUserId() {
    const sub = this.route.parent.parent.paramMap
      .subscribe((params) => {
          this.userId = params.get('userId');
      });

    this.activeSubs.push(sub);
  }

  onNewShelf() {
    const dialogRef = this.dialog.open(NewShelfDialogComponent,
      {
        height: '500px'
      });

    const sub = dialogRef.afterClosed()
      .subscribe(res => {
        if (res) {
          if (res.newShelf) {
            this.shelvesService.onNewShelfAdded();
          }
        }
    });

    this.activeSubs.push(sub);
  }

  ngOnDestroy(): void {
    this.dialog.closeAll();

    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
