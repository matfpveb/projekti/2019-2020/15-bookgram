import { UserService } from 'src/app/users/user/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdvancedValidators } from 'ng-validator';
import { Subscription } from 'rxjs';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  templateUrl: './password-change-dialog.component.html',
  styleUrls: ['./password-change-dialog.component.css']
})
export class PasswordChangeDialogComponent implements OnInit, OnDestroy {
  public isLoading: boolean;
  public form: FormGroup;
  public error: string;
  private activeSubs: Subscription[];

  constructor(private dialogRef: MatDialogRef<PasswordChangeDialogComponent>,
              private formBuilder: FormBuilder,
              private userService: UserService) {
    this.isLoading = false;
    this.error = '';
    this.activeSubs = [];

    this.form = formBuilder.group({
      password: ['', [Validators.required]],
      password2: ['', [Validators.required, AdvancedValidators.equalsToField('password', 'listener')]],
      oldPassword: ['', Validators.required]
    });

    const sub = this.oldPassword.valueChanges.subscribe(() => {
      if (this.error) {
        this.error = '';
      }
    });

    this.activeSubs.push(sub);
  }

  ngOnInit(): void { }

  onSubmit() {
    if (this.form.valid && !this.error) {
      this.isLoading = true;

      const data = {
        password: this.password.value,
        oldPassword: this.oldPassword.value
      };

      const sub = this.userService.changePassword(data)
      .subscribe(res => {
        this.isLoading = false;

        this.dialogRef.close();
      }, error => {
        this.isLoading = false;

        this.error = error;
      });

      this.activeSubs.push(sub);
    }
  }

  public get password() {
    return this.form.get('password');
  }
  public get password2() {
    return this.form.get('password2');
  }
  public get oldPassword() {
    return this.form.get('oldPassword');
  }

  ngOnDestroy() {
    AdvancedValidators.destroyListener('listener');

    this.activeSubs.forEach(sub => sub.unsubscribe());
  }
}
