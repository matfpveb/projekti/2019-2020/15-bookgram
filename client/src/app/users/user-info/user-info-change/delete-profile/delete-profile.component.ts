import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/users/user/user.service';

@Component({
  templateUrl: './delete-profile.component.html',
  styleUrls: ['./delete-profile.component.css']
})
export class DeleteProfileComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public error: string;
  public isLoading: boolean;
  private activeSubs: Subscription[];

  constructor(private dialogRef: MatDialogRef<DeleteProfileComponent>,
              private formBuilder: FormBuilder,
              private userService: UserService) {
    this.activeSubs = [];
    this.error = '';

    this.form = this.formBuilder.group({
      password: ['', Validators.required]
    });

    const sub = this.form.valueChanges.subscribe((data) => {
      if (this.error) {
        this.error = '';
      }
    });

    this.activeSubs.push(sub);
   }

  ngOnInit(): void { }

  onUsernameSave() {
    this.isLoading = true;

    const data = {
      password: this.password.value
    };

    const sub = this.userService.deleteUser(data)
      .subscribe(res => {
        this.isLoading = false;

        this.dialogRef.close({userDeleted: true});
      }, error => {
        this.isLoading = false;

        this.error = error;
      });

    this.activeSubs.push(sub);
  }

  public get password() {
    return this.form.get('password');
  }

  ngOnDestroy(): void {
    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
