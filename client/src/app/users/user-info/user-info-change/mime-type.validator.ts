// https://mydev.life/2019/04/19/asynchronous-mime-type-validator-for-file-inputs-in-angular-reactive-forms/
import { AbstractControl } from '@angular/forms';
import {Observable, Observer, of} from 'rxjs';

export const mimeType = (
  control: AbstractControl
): Promise<{ [key: string]: any }> | Observable<{ [key: string]: any }> => {
  if (typeof(control.value) === 'string') {
    return of(null);
  }
  const file = control.value as File;
  const fr = new FileReader();
  const frObs = new Observable(
    (observer: Observer<{ [key: string]: any }>) => {
      fr.addEventListener('loadend', () => {
        // @ts-ignore
        const arr = new Uint8Array(fr.result).subarray(0, 4);

        let isValid = false;

        const header = arr.reduce((accumulator, currValue) => {
          return accumulator + currValue.toString(16);
        }, '');

        switch (header) {
          case '89504e47':
            isValid = true;
            break;
          case 'ffd8ffe0':
          case 'ffd8ffe1':
          case 'ffd8ffe2':
          case 'ffd8ffe3':
          case 'ffd8ffe8':
            isValid = true;
            break;
          default:
            isValid = false;
            break;
        }
        observer.next(isValid ? null : { invalidMimeType: true });

        observer.complete();
      });
      fr.readAsArrayBuffer(file);
    }
  );
  return frObs;
};
