import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonValidators } from 'ng-validator';
import { UserService } from 'src/app/users/user/user.service';

@Component({
  selector: 'app-username-change-dialog',
  templateUrl: './username-change-dialog.component.html',
  styleUrls: ['./username-change-dialog.component.css']
})
export class UsernameChangeDialogComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public error: string;
  public isLoading: boolean;
  private activeSubs: Subscription[];

  constructor(private dialogRef: MatDialogRef<UsernameChangeDialogComponent>,
              private formBuilder: FormBuilder,
              private userService: UserService) {
    this.activeSubs = [];
    this.error = '';

    this.form = this.formBuilder.group({
      username: ['', CommonValidators.requiredTrim],
      password: ['', Validators.required]
    });

    const sub = this.form.valueChanges.subscribe((data) => {
      if (this.error) {
        this.error = '';
      }
    });

    this.activeSubs.push(sub);
   }

  ngOnInit(): void { }

  onUsernameSave() {
    this.isLoading = true;

    const data = {
      username: this.username.value.trim(),
      password: this.password.value
    };

    const sub = this.userService.changeUsername(data)
      .subscribe(res => {
        this.isLoading = false;

        this.dialogRef.close({usernameChanged: true});
      }, error => {
        this.isLoading = false;

        this.error = error;
      });

    this.activeSubs.push(sub);
  }

  public get username() {
    return this.form.get('username');
  }
  public get password() {
    return this.form.get('password');
  }

  ngOnDestroy(): void {
    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
