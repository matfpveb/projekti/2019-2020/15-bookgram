import { AuthService } from './../../../auth/auth.service';
import { PasswordChangeDialogComponent } from './password-change-dialog/password-change-dialog.component';
import { UsernameChangeDialogComponent } from './username-change-dialog/username-change-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UserService } from '../../user/user.service';
import { DatePipe } from '@angular/common';
import { mimeType } from './mime-type.validator';
import { User } from '../../user.model';
import { DeleteProfileComponent } from './delete-profile/delete-profile.component';

@Component({
  selector: 'app-user-info-change',
  templateUrl: './user-info-change.component.html',
  styleUrls: ['./user-info-change.component.css'],
  providers: [DatePipe]
})
export class UserInfoChangeComponent implements OnInit, OnDestroy {
  @Input() user: User;
  public profileForm: FormGroup;
  public imagePath: string;
  private activeSubs: Subscription[];

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private datePipe: DatePipe,
              private dialog: MatDialog,
              private authService: AuthService) {
    this.activeSubs = [];
    this.imagePath = '../../../../assets/images/monkey-reading.jpg';

    this.profileForm = this.formBuilder.group({
      firstName: [''],
      lastName: [''],
      image: ['', [], mimeType],
      birthday: [''],
      about: ['']
    });
  }

  ngOnInit(): void {
    this.updateInfo();
  }

  updateInfo() {
    if (this.user.imagePath) {
      this.imagePath = this.user.imagePath;
    }

    this.user.birthday = this.datePipe.transform(this.user.birthday, 'yyyy-MM-dd');

    for (const property in this.user) {
      if (this.user[property]) {
        this.profileForm.patchValue(
          { [property]: this.user[property] });
      }
    }
  }

  onImageSelected(event: Event) {
    if ((event.target as HTMLInputElement).files !== undefined) {
      const file = (event.target as HTMLInputElement).files[0];

      if (file.name !== undefined && file.name !== '') {
        this.profileForm.patchValue({ image: file });
        this.image.updateValueAndValidity();

        const fr = new FileReader();
        fr.onload = () => {
          this.imagePath = fr.result as string;
        };
        fr.readAsDataURL(file);
      }
    }
  }

  onSaveProfile(data) {
    const body = new FormData();

    // tslint:disable-next-line: forin
    for (const property in this.profileForm.controls) {
      const obj = this.profileForm.get(property);

      if (property !== 'image') {
        if (obj.touched && obj.value.trim() !== this.user[property]) {
          body.append(property, obj.value.trim());
           // body[property] = obj.value.trim();
        }
      }
    }

    if (this.image.value && this.image.valid) {
      // body.image = this.image.value;
      body.append('image', this.image.value);
    }

    const sub = this.userService.changeUserInfo(body)
      .subscribe((res: { msg: string }) => {
        alert(res.msg);
      });

    this.activeSubs.push(sub);
  }

  onOpenChangeUsernameDialog() {
    const dialogRef = this.dialog.open(UsernameChangeDialogComponent);

    const sub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.usernameChanged) {
          this.userService.onUserChanged();
        }
      }
    });

    this.activeSubs.push(sub);
  }

  onOpenDeleteUserDialog() {
    const dialogRef = this.dialog.open(DeleteProfileComponent);

    const sub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.userDeleted) {
          this.authService.logout();
        }
      }
    });

    this.activeSubs.push(sub);
  }

  onOpenPasswordChangeDialog() {
    this.dialog.open(PasswordChangeDialogComponent);
  }

  public get image() {
    return this.profileForm.get('image');
  }

  public get firstName() {
    return this.profileForm.get('firstName');
  }

  public get lastName() {
    return this.profileForm.get('lastName');
  }

  public get birthday() {
    return this.profileForm.get('birthday');
  }

  public get about() {
    return this.profileForm.get('about');
  }

  ngOnDestroy(): void {
    this.dialog.closeAll();

    this.activeSubs.forEach(sub => sub.unsubscribe());
  }
}
