import { Book } from './../../../books/books.model';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from './../../users.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { map, switchMap } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recommended-books',
  templateUrl: './recommended-books.component.html',
  styleUrls: ['./recommended-books.component.css']
})
export class RecommendedBooksComponent implements OnInit, OnDestroy {
  public books: Book[];
  private activeSubs: Subscription[];

  constructor(private route: ActivatedRoute,
              private usersService: UsersService) {
    this.activeSubs = [];

    this.getRecommendedBooks();
  }

  getRecommendedBooks() {
    const sub = this.route.parent.paramMap
    .pipe(
      map((params) => params.get('userId')),
      switchMap(userId =>
        this.usersService.getRecommendedBooks(userId)
      )
    )
    .subscribe((res: { books: Book[]}) => {
      if (res) {
        // Da prikaze prva dva reda preporucenih knjiga ako ih ima vise
      const n = 10 < res.books.length ? 10 : res.books.length;
      this.books = res.books.slice(0, n);
      } else {
        this.books = [];
      }
    });

    this.activeSubs.push(sub);
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
