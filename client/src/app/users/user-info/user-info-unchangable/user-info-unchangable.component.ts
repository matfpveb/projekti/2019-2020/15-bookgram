import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../user.model';

@Component({
  selector: 'app-user-info-unchangable',
  templateUrl: './user-info-unchangable.component.html',
  styleUrls: ['./user-info-unchangable.component.css']
})
export class UserInfoUnchangableComponent implements OnInit {
  @Input() user: User;
  constructor() { }

  ngOnInit(): void {
  }

}
