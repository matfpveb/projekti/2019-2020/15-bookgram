import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpErrorHandler } from 'src/app/utils/http-error-handler.model';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import * as jwt_decode from 'jwt-decode';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService extends HttpErrorHandler {
  private readonly userUrl = 'http://localhost:3000/users/user/';
  // tslint:disable: variable-name
  private _userChangeListener = new Subject<boolean>();

  constructor(router: Router,
              private http: HttpClient) {
    super(router);
  }

  public get userChangeListener() {
    return this._userChangeListener.asObservable();
  }

  getUserId() {
    const token = localStorage.getItem('token');
    const decodedToken = jwt_decode(token);

    return decodedToken.id;
  }

  onUserChanged() {
    this._userChangeListener.next(true);
  }

  changeUserInfo(body) {
    return this.http.post(this.userUrl, body)
      .pipe(catchError(super.handleError()));
  }

  changeUsername(body) {
    return this.http.post(this.userUrl + 'username', body)
      .pipe(catchError(super.handleError()));
  }

  changePassword(body) {
    return this.http.post(this.userUrl + 'password', body)
      .pipe(catchError(super.handleError()));
  }

  getUserByd(userId) {
    return this.http.get(this.userUrl + `${userId}`)
      .pipe(catchError(super.handleError()));
  }

  getShelvesByBook(bookId) {
    return this.http.get(this.userUrl + `shelves/${bookId}`)
      .pipe(catchError(super.handleError()));
  }

  getRatingByBook(bookId) {
    return this.http.get(this.userUrl + `ratings/${bookId}`)
      .pipe(catchError(super.handleError()));
  }

  checkIfGenreInFavourites(genreId) {
    return this.http.get(this.userUrl + `favouriteGenres/${genreId}`)
      .pipe(catchError(super.handleError()));
  }

  removeGenreFromFavourites(body) {
    return this.http.request('delete', this.userUrl + `favouriteGenres`, { body })
      .pipe(catchError(super.handleError()));

  }

  addGenreToFavourites(body) {
    return this.http.post(this.userUrl + `favouriteGenres`, body)
      .pipe(catchError(super.handleError()));
  }

  deleteUser(body: { password: string}) {
    return this.http.request('delete', this.userUrl, { body })
      .pipe(catchError(super.handleError()));
  }
}
