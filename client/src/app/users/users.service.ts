import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpErrorHandler } from '../utils/http-error-handler.model';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends HttpErrorHandler {
  private readonly usersUrl = 'http://localhost:3000/users/';

  constructor(router: Router,
              private http: HttpClient) {
    super(router);
  }

  getUserById(userId: string) {
    return this.http.get(this.usersUrl + `${userId}`)
      .pipe(catchError(super.handleError()));
  }

  getShelves(userId: string) {
    return this.http.get(this.usersUrl + `${userId}/shelves`)
      .pipe(catchError(super.handleError()));
  }

  getFavouriteGenres(userId: string) {
    return this.http.get(this.usersUrl + `${userId}/favouriteGenres`)
      .pipe(catchError(super.handleError()));
  }

  getRecommendedBooks(userId: string) {
    return this.http.get(this.usersUrl + `${userId}/recommendedBooks`)
      .pipe(catchError(super.handleError()));
  }

  getAllUserDiscussions(userId: string, options) {
    return this.http.get(this.usersUrl + `${userId}/discussions`, { params: options })
      .pipe(catchError(super.handleError()));
  }

  searchUserDiscussions(userId: string, options) {
    return this.http.get(this.usersUrl + `${userId}/discussions/search`, { params: options })
    .pipe(catchError(super.handleError()));
  }

}
